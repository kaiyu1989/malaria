<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Quiz Response Model
 *
 * A table stores students quiz responses, tracks student's 
 * answers to each question in a quiz
 *
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class QuizResponse extends Model {

	/**
	 * Fillable fields for a Quiz Response
	 *
	 * @var array
	 */
	protected $fillable = [
		'quiz_id',
		'user_id',
		'choice_id',
		'question_id',
		'quiz_attempt_id'
	];

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A response is owned by a quiz.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function quiz(){
		return $this->belongsTo('App\Quiz');
	}

	/**
	 * A response is owned by a choice.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function choice(){
		return $this->belongsTo('App\Choice');
	}

	/**
	 * A response is owned by a user.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
		return $this->belongsTo('App\User');
	}

	/**
	 * A response is owned by a question.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function question(){
		return $this->belongsTo('App\Question');
	}

	/**
	 * A response is owned by a QuizAttempt which is user quizzes they have taken.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function quizAttempt(){
		return $this->belongsTo('App\quizAttempt');
	}
}
