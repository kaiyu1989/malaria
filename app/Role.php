<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Role extends Model {

	/**
	 * Fillable fields for a Quiz
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
		'name',
		'description'
	];

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A role can be assigned to many users
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function users()
    {
        return $this->hasMany('App\User', 'role_id', 'id');
    }

    /**
	 * When fetching the users for CRUD, current user will only fetch users that has lower privilege for them.
	 * 
	 * @return array list of roles
	 */
	public function scopeRoleList($query){
		if(!Auth::user()->isSuperAdmin()) {
			return $query->where('id','>',Auth::user()->role_id)->lists('name','id');
		} else {
			return $query->lists('name','id');
		}
	}
}