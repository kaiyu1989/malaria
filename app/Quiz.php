<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Quiz Model
 *
 * A table stores quizzes, either general or country based
 *
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class Quiz extends Model {

	/**
	 * Fillable fields for a Quiz
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'user_id',
		'country_code'
	];

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A quiz is owned by a user.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
		return $this->belongsTo('App\User');
	}

	/**
	 * A quiz is owned by a school.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function organisation(){
		return $this->belongsTo('App\Organisation');
	}

	/**
	 * A quiz can have many questions
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function questions(){
		return $this->hasMany('App\Question');
	}

	/**
	 * A quiz can have many quiz responses
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizResponses(){
		return $this->hasMany('App\QuizResponse');
	}

	/**
	 * A quiz is owned by a country.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function country(){
		return $this->belongsTo('App\Country','country_code','id');
	}

	/**
	 * A quiz can have many quiz attemps
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizAttempts(){
		return $this->hasMany('App\QuizAttempt');
	}

	/**
	 * A list of general quizzes
	 * 
	 * @return array list of general quizzes
	 */
	public function scopeGeneralQuiz($query){
		return $query->whereNull('country_code')->latest()->get();
	}

	/**
	 * Quiz of the day
	 * 
	 * @return array list of general quizzes
	 */
	public function scopeQuizOfTheDay($query){
		return $query->orderByRaw('RAND()')->first();
	}
}
