<?php namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Config;
class Organisation extends Model {
	use SoftDeletes;

	/**
	 * Fillable fields for an organisation
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
		'name',
		'description'
	];

	/**
	 * Additional Field to treat this as Carbon instances
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A school can have many users
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function users(){
		return $this->hasMany('App\User');
	}

	/**
	 * A school can have many quizzes
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizzes(){
		return $this->hasMany('App\Quiz');
	}

	/**
	 * school has many classes
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function classes(){
		return $this->hasMany('App\StudentClass');
	}

	/**
	 * fetch all classes when user is a school staff otherwise, his class only
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function classesByPrivilege() {
		if(Auth::user()->isTeacher()){
			return $this->classes()->whereTeacherId(Auth::id());
		} else {
			return $this->classes();
		}
	}

	/**
	 * When fetching the users for CRUD, current user will only fetch users that has lower privilege for them.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function usersByPrivilege(){
		if(Auth::user()->role_id != 1){
			return $this->users()->where('role_id','>',Auth::user()->role_id)->orderBy('role_id');
		} else {
			return $this->users()->orderBy('role_id');
		}
	}

	/**
	 * An organisation can have many informations
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function information() {
		return $this->hasMany('App\Information');
	}

	/**
	 * fetaching school admin list
	 * @return list of teachers
	 */
	public function admins(){
		$id = Config::get('malaria.role.admin_id');
		return $this->users()->whereRoleId($id);
	}	

	/**
	 * fetaching school staff list
	 *
	 * @return list of teachers
	 */
	public function schools(){
		$id = Config::get('malaria.role.staff_id');
		return $this->users()->whereRoleId($id);
	}

	/**
	 * fetaching teacher list
	 *
	 * @return list of teachers
	 */
	public function teachers(){
		$id = Config::get('malaria.role.teacher_id');
		return $this->users()->whereRoleId($id);
	}

	/**
	 * fetaching student list
	 *
	 * @return list of students
	 */
	public function students(){
		$id = Config::get('malaria.role.student_id');
		return $this->users()->whereRoleId($id);
	}
}
