<?php namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);

		$router->bind('quizzes',function($id) {
			return \App\Quiz::findOrFail($id);
		});

		$router->bind('questions',function($id) {
			return \App\Question::findOrFail($id);
		});

		$router->bind('users',function($id) {
			return \App\User::findOrFail($id);
		});

		$router->bind('classes',function($id) {
			return \App\StudentClass::findOrFail($id);
		});

		$router->bind('notes',function($id) {
			return \App\Information::findOrFail($id);
		});

		$router->bind('schools',function($id) {
			return \App\Organisation::findOrFail($id);
		});

		$router->bind('country',function($id) {
			return \App\Country::findOrFail($id);
			//return \App\Country::where('iso2', '=', $iso2)->firstOrFail();
		});
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => $this->namespace], function($router)
		{
			require app_path('Http/routes.php');
		});
	}

}
