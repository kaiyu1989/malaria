<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Country Report Model
 *
 * A table stores Country Reports of Malaria deathes from WHO
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class CountryReport extends Model {
	protected $table = 'country_report';
	
	public $timestamps = false;
	/**
	 * Fillable fields for a QuizScore
	 *
	 * @var array
	 */
	protected $fillable = [
		'country_code',
		'death',
		'cases',
		'estimate_death',
		'estimate_cases',
		'year'
	];

	/**
	 * A country report belongs to a country
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function countryReport() {
		return $this->belongsTo('App\Country','country_code','id');
	}
}
