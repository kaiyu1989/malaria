<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model {
	protected $table = "classes";
	/**
	 * Fillable fields for a class
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
		'name',
		'organisation_id',
		'teacher_id'
	];

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

    /**
	 * A class is owned by an organisation.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function organisation() {
		return $this->belongsTo('App\Organisation');
	}

	/**
	 * A class have many students
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function students() {
		return $this->belongsToMany('App\User','class_student','class_id','student_id');
	}

	/**
	 * A class has one teacher
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\hasOne
	 */
	public function teacher() {
		return $this->hasOne('App\User','id','teacher_id');
	}
}
