<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Quiz Model
 *
 * A table stores either general or country based
 *
 * @author     Kai Yu
 * @since      Delivery Cycle three
 */
class Information extends Model {

	/**
	 * Fillable fields for country or general information
	 *
	 * @var array
	 */
	protected $fillable = [
		'information',
		'organisation_id',
		'country_code'
	];


	/**
	 * allow nullable foriegn key
	 *
	 * @param $value
	 */
	public function setOrganisationIdAttribute($organisation_id)
	{
		$this->attributes['organisation_id'] = $organisation_id ? $organisation_id : null;
	}

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * Information can be owned by a country.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function country(){
		return $this->belongsTo('App\Country','country_code','id');
	}

	/**
	 * Organisation can have their own information.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function organisation(){
		return $this->belongsTo('App\Organisation');
	}

}
