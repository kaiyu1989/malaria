<?php
// Home
Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Home', action('HomeController@index'));
});

// Home > Quizzes
Breadcrumbs::register('quizzes', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Quizzes', action('QuizzesController@index'));
});

// Home > Quizzes > [Quiz]
Breadcrumbs::register('quiz', function($breadcrumbs,$quiz)
{
	$breadcrumbs->parent('quizzes');
    $breadcrumbs->push($quiz->title, action('QuizzesController@show',$quiz));
});

// Home > Quizzes > Create
Breadcrumbs::register('quiz_create', function($breadcrumbs)
{
	$breadcrumbs->parent('quizzes');
    $breadcrumbs->push('Create Quiz', action('QuizzesController@create'));
});

// Home > Quizzes > Edit
Breadcrumbs::register('quiz_edit', function($breadcrumbs,$quiz)
{
	$breadcrumbs->parent('quiz',$quiz);
    $breadcrumbs->push('Edit', action('QuizzesController@create'));
});

// Home > Quizzes > [Quiz] > Questions
Breadcrumbs::register('questions', function($breadcrumbs,$quiz)
{
	$breadcrumbs->parent('quiz', $quiz);
    $breadcrumbs->push('Questions', action('QuestionsController@index',$quiz));
});

// Home > Quizzes > [Quiz] > Questions > [Question]
Breadcrumbs::register('question', function($breadcrumbs,$quiz,$question)
{
	$breadcrumbs->parent('questions', $quiz);
    $breadcrumbs->push($question->question, action('QuestionsController@show',['quiz'=>$quiz,'question'=>$question]));
});

// Home > Quizzes > [Quiz] > Questions > [Question] > Create
Breadcrumbs::register('question_create', function($breadcrumbs,$quiz)
{
	$breadcrumbs->parent('quiz',$quiz);
    $breadcrumbs->push('Add Question', action('QuestionsController@create',['quiz'=>$quiz]));
});

// Home > Quizzes > Question > Create
Breadcrumbs::register('question_edit', function($breadcrumbs,$quiz,$question)
{
	$breadcrumbs->parent('question',$quiz,$question);
    $breadcrumbs->push('Edit', action('QuestionsController@edit',['quiz'=>$quiz,'question'=>$question]));
});

// Home > User
Breadcrumbs::register('user_profile', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Profile', action('UserController@getProfile'));
});

// Home > User > Profile edit
Breadcrumbs::register('user_profile_edit', function($breadcrumbs)
{
    $breadcrumbs->parent('user_profile');
    $breadcrumbs->push('Edit Profile', action('UserController@getProfileEdit'));
});

// Home > User > Profile password
Breadcrumbs::register('user_profile_password', function($breadcrumbs)
{
    $breadcrumbs->parent('user_profile');
    $breadcrumbs->push('Edit Profile', action('UserController@getProfilePassword'));
});

// Home > Quiz Map
Breadcrumbs::register('user_map', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Quiz Map', action('UserController@getMap'));
});

// Home > Quiz Map
Breadcrumbs::register('user_general_quiz', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('General Quizzes', action('UserController@getQuizzes'));
});

// Home > Country Quiz
Breadcrumbs::register('user_country_quiz', function($breadcrumbs,$country)
{
    $breadcrumbs->parent('user_map');
    $breadcrumbs->push($country->country_name, action('UserController@getCountryQuizzes',$country->iso2));
});

// Home > Country Quiz > {Country}
Breadcrumbs::register('user_country_quiz_attempts', function($breadcrumbs,$quiz)
{
    if($quiz->country) {
        $breadcrumbs->parent('user_country_quiz',$quiz->country);
        $breadcrumbs->push($quiz->title, action('UserController@getQuizAttempts',$quiz->id));
    } else {
        $breadcrumbs->parent('home');
        $breadcrumbs->push($quiz->title, action('UserController@getQuizAttempts',$quiz->id));
    }
});

// Home > Country Quiz > {Country} > Attempt Quiz
Breadcrumbs::register('user_country_quiz_attempt', function($breadcrumbs,$quiz)
{
    $breadcrumbs->parent('user_country_quiz_attempts',$quiz);
    $breadcrumbs->push('Attempt Quiz', action('UserController@getQuizAttempt',$quiz->id));
});

// Home > Country Quiz > {Country} > Review Quiz
Breadcrumbs::register('user_country_quiz_review', function($breadcrumbs,$quiz)
{
    $breadcrumbs->parent('user_country_quiz_attempts',$quiz);
    $breadcrumbs->push('Review Attempt', action('UserController@getQuizReview',$quiz->id));
});

// Home > Users
Breadcrumbs::register('users', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Users', action('UsersController@index'));
});

// Home > Users > Show User
Breadcrumbs::register('users_show', function($breadcrumbs,$user)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push($user->name, action('UsersController@show',$user));
});

// Home > Users > Create User
Breadcrumbs::register('users_create', function($breadcrumbs)
{
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Create User', action('UsersController@create'));
});

// Home > Users > Edit User
Breadcrumbs::register('users_edit', function($breadcrumbs,$user)
{
    $breadcrumbs->parent('users_show',$user);
    $breadcrumbs->push('Edit User', action('UsersController@edit',$user));
});

// Home > Classes
Breadcrumbs::register('classes', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Classes', action('ClassesController@index'));
});

// Home > Classes > Create Class
Breadcrumbs::register('classes_create', function($breadcrumbs)
{
    $breadcrumbs->parent('classes');
    $breadcrumbs->push($class->name, action('ClassesController@show',$class));
});

// Home > Classes > Show Class
Breadcrumbs::register('classes_show', function($breadcrumbs,$class)
{
    $breadcrumbs->parent('classes');
    $breadcrumbs->push($class->name, action('ClassesController@show',$class));
});

// Home > Classes > Edit Class
Breadcrumbs::register('classes_edit', function($breadcrumbs,$class)
{
    $breadcrumbs->parent('classes_show',$class);
    $breadcrumbs->push('Edit', action('ClassesController@show',$class));
});


// Home > Classes > Class Students
Breadcrumbs::register('classes_students', function($breadcrumbs,$class)
{
    $breadcrumbs->parent('classes_show',$class);
    $breadcrumbs->push('Class Students', action('ClassesController@getStudents',$class));
});

// Home > Classes > Class Results
Breadcrumbs::register('classes_results', function($breadcrumbs,$class)
{
    $breadcrumbs->parent('classes_show',$class);
    $breadcrumbs->push('Class Results', action('ClassesController@getClassResults',$class));
});

// Home > Roles > Staff
Breadcrumbs::register('roles_staff', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('School Staff', action('RolesController@getStaff'));
});

// Home > Roles > Teacher
Breadcrumbs::register('roles_teacher', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Teachers', action('RolesController@getTeacher'));
});

// Home > Roles > Student
Breadcrumbs::register('roles_student', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Students', action('RolesController@getStudent'));
});

// Home > Roles > {role} > Detail
Breadcrumbs::register('roles_user_detail', function($breadcrumbs,$role_name,$user)
{
    $breadcrumbs->parent('roles_'.$role_name);
    $breadcrumbs->push($user->name, action('RolesController@getUser',$user));
});

// Home > Roles > {role} > Create
Breadcrumbs::register('roles_user_create', function($breadcrumbs,$role_name)
{
    $breadcrumbs->parent('roles_'.$role_name);
    $breadcrumbs->push('Create Account', action('RolesController@getCreateUser'));
});

// Home > Roles > {role} > Edit Info
Breadcrumbs::register('roles_user_edit', function($breadcrumbs,$role_name,$user)
{
    $breadcrumbs->parent('roles_user_detail', $role_name, $user);
    $breadcrumbs->push('Edit Information', action('RolesController@getEditUser',$user));
});

// Home > Roles > {role} > Edit Password
Breadcrumbs::register('roles_user_password', function($breadcrumbs,$role_name,$user)
{
    $breadcrumbs->parent('roles_user_detail', $role_name, $user);
    $breadcrumbs->push('Edit Password', action('RolesController@getEditPassword',$user));
});

// Home > Roles > {role} > Quiz Results
Breadcrumbs::register('roles_user_quiz_results', function($breadcrumbs,$role_name,$user)
{
    $breadcrumbs->parent('roles_user_detail', $role_name, $user);
    $breadcrumbs->push('Quiz Results', action('RolesController@getQuizResults',$user));
});

// Home > Roles > {role} > Quiz Results > Quiz Attempt
Breadcrumbs::register('roles_user_quiz_attempts', function($breadcrumbs,$role_name,$user,$attempt_id)
{
    $breadcrumbs->parent('roles_user_quiz_results', $role_name, $user);
    $breadcrumbs->push('Quiz Attempt', action('RolesController@getQuizAttempt',$attempt_id));
});













