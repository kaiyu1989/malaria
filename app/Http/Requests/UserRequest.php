<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Illuminate\Validation\Factory;
use Hash;

/**
 * User table validation rules
 *
 * A customized request rules for user forms
 * @author     Kai Yu
 * @since      Delivery Cycle Two
 */
class UserRequest extends Request {
	/**
	 * constructor
	 * bind a function to check user's input password matches current passward in storage
	 */
	public function __construct(Factory $factory)
    {
        $factory->extend('passcheck', function ($attribute, $value, $parameters) 
			{
			    return Hash::check($value, Auth::user()->getAuthPassword());
			},
			'The current password is not correct.'
		);
    }

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		//a hidden field to determine the form type 
		//whether is a general information form or password reset form
		if( !$this->get('is_general') ){		
			return [
				'password' => 'required|min:6|passcheck',
				'new_password' => 'required|different:password|min:6|confirmed'
			];
		} else {
			return [
				'first_name' => 'required|max:255|alpha_space',
				'last_name' => 'required|max:255|alpha_space',
				'email' => 'required|email|max:255|unique:users,email,'.Auth::user()->id,
				'avatar' => 'image|max:2000'
			];
		}
	}
}
