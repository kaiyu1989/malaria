<?php namespace App\Http\Controllers;


use App\Quiz;
use App\Question;
use App\Country;
use App\Answer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;

/**
 * Quizzes Controller
 *
 * This controller renders your application's "quizzes" for users that are authenticated.
 * @author     Kai Yu
 * @since      Delivery Cycle Two
 */
class QuizzesController extends Controller {

	/**
	 * rules for the quiz form
	 */
	protected $rules = [
		'title' => 'required|min:3|max:255',
	];

	/**
	 * Display a listing of the quizzes.
	 *
	 * @return Response
	 */
	public function index()
	{
		$quizzes = Quiz::latest()->get();
		return view('quizzes.index',compact('quizzes'));
	}

	/**
	 * Show the form for creating a new quiz.
	 *
	 * @return Response
	 */
	public function create()
	{
		$countries = Country::orderBy('country_name')->lists('country_name','id');
		return view('quizzes.create',compact('countries'));
	}

	/**
	 * Display the specified quiz.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Quiz $quiz)
	{
		return view('quizzes.show',compact('quiz'));
	}

	/**
	 * Store a newly created quiz in storage.
	 *
	 * @param Request $request current request instance of the HTTP request 
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,$this->rules);

		if( !$request->has('is_general') ) {
			$request['country_code'] = null;
		}
		$quiz = Auth::user()->quizzes()->create($request->all());

		flash()->success('Quiz : '.$quiz->title.' has been created!');
		return redirect('quizzes');
	}

	/**
	 * Show the form for editing the specified quiz.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Quiz $quiz)
	{
		$countries = Country::orderBy('country_name')->lists('country_name','id');
		return view('quizzes.edit',compact('quiz','countries'));
	}

	/**
	 * Update the specified quiz in storage.
	 *
	 * @param  Quiz $quiz current quiz instance
	 * @param Request $request current request instance of the HTTP request 
	 * @return Response
	 */
	public function update(Quiz $quiz,Request $request)
	{
		$this->validate($request,$this->rules);
		
		if( !$request->has('is_general') ) {
			$request['country_code'] = null;
		}

		$quiz->update($request->all());
		flash()->success('Quiz : '.$quiz->title.' has been updated!');
		return redirect()->route('quizzes.show', [$quiz]);
	}

	/**
	 * Remove the specified quiz from storage.
	 *
	 * @param  Quiz $quiz current quiz instance
	 * @return Response
	 */
	public function destroy(Quiz $quiz)
	{
		$quiz->delete();
		flash()->success('You have successfully deleted a quiz!');
		return redirect('quizzes');
	}

}
