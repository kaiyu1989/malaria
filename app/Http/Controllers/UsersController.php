<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Organisation;
use App\Role;
use Input;
/**
 * Users Controller
 *
 * This controller renders your application's "users" for users that are authenticated.
 * @author     Kai Yu
 * @since      Delivery Cycle Two
 */
class UsersController extends Controller {
	/**
	 * rules for the user form
	 */
	protected $rules = [
		'first_name' => 'required|max:255|alpha_space',
		'last_name' => 'required|max:255|alpha_space',
		'password' => 'required|min:6|confirmed',
		'email' => 'required|email|max:255|unique:users',
		'role_id' => 'required',
	];

	/**
	 * Display a listing of the users.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = Auth::user()->organisation->usersByPrivilege()->get();
		return view('users.index',compact('users'));
	}

	/**
	 * Show the form for creating a new user.
	 *
	 * @return Response
	 */
	public function create()
	{
		$role = Input::get('role');
		$roles = Role::roleList();
		return view('users.create',compact('roles','role'));
	}

	/**
	 * Store a newly created user in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,$this->rules);

		$request['organisation_id'] = Auth::user()->organisation_id;
		$request['password'] = bcrypt($request->input('password'));

		$user = User::create($request->all());

		flash()->success('User '.$user->name.' has been created!');
		return redirect('users');
	}

	/**
	 * Display the specified user.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function show(User $user)
	{
		//redirect if the permission is lower
		if(Auth::user()->role_id > $user->role_id) {
			flash()->error('Please grant permission to view this user.');
			return redirect('users');
		}
		return view('users.show',compact('user'));
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function edit(User $user)
	{
		//redirect if the permission is lower
		if(Auth::user()->role_id > $user->role_id) {
			flash()->error('Please grant permission to edit this user.');
			return redirect('users');
		}
		$roles = Role::roleList();
		return view('users.edit',compact('user','roles'));
	}

	/**
	 * Update the specified user in storage.
	 *
	 * @param Request $request
	 * @param  User $user
	 * @return Response
	 */
	public function update(User $user,Request $request)
	{
		$rules = array_except($this->rules, array('password'));
		$rules['email'] = 'required|email|max:255|unique:users,email,'.$user->id;
		$this->validate($request,$rules);

		$user->update($request->all());
		flash()->success('User '.$user->name.'\'s detail has been updated!');
		return redirect('users');
	}

	/**
	 * Remove the specified user from storage.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function destroy(User $user)
	{
		$user->delete();
		flash()->success('You have successfully deleted a user!');
		return redirect('users');
	}

	/**
	 * Show the form for editing the specified user's password.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function editPassword(User $user)
	{
		//redirect if the permission is lower
		if(Auth::user()->role_id > $user->role_id) {
			flash()->error('Please grant permission to edit this user.');
			return redirect('users');
		}
		$roles = Role::roleList();
		return view('users.edit-password',compact('user','roles'));
	}

	/**
	 * Update the specified user's password in storage.
	 *
	 * @param Request $request
	 * @param  User $user
	 * @return Response
	 */
	public function updatePassword(User $user,Request $request)
	{
		$rules = array_except($this->rules, array('name','email','role_id'));
		$this->validate($request,$rules);

		$request['password'] = bcrypt($request->input('password'));
		$user->update($request->all());
		flash()->success('User '.$user->name.'\'s password has been updated!');
		return redirect('users');
	}

}
