<?php namespace App\Http\Controllers;

use App\Quiz;
use App\Country;
use App\User;
use App\StudentClass;
use App\Organisation;
use Auth;
use Config;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::user()->isSuperAdmin()){
			$schools = Organisation::all();
			return view('home',compact('schools'));
		} elseif(Auth::user()->isAdmin()) {
			return view('home');
		} elseif(Auth::user()->isSchool()) {
			return view('home');
		} elseif(Auth::user()->isTeacher()) {
			return view('home');
		} else {
			$country = Country::countryOfTheDay();
			return view('home',compact('country'));
		}
	}

	/**
	 * Show information slides to learn malaria
	 *
	 * @return Response
	 */
	public function information()
	{
		return view('notes');
	}

	/**
	 * download backups form
	 *
	 * @return Response
	 */
	public function backup()
	{
		return view('backup');
	}

	/**
	 * dump backups
	 *
	 * @return file
	 */
	public function dumpBackUp()
	{
		$host = Config::get('database.connections.mysql.host');
        $database = Config::get('database.connections.mysql.database');
        $username = Config::get('database.connections.mysql.username');
        $password = Config::get('database.connections.mysql.password');
        $backupPath = app_path() . "/../storage/backup/";
        $backupFileName = $database . "-" . date("Y-m-d-H-i-s") . '.sql';
 
        //for linux replace the path with /usr/local/bin/mysqldump (The path might varies).
       	$path = "/usr/bin/mysqldump";
 
        //without password
        //$command = $path . " -u " . $username . " " . $database . " > " . $backupPath . $backupFileName;
        //with password
        $command = $path ." -h " . $host. " -u " . $username . " -p" . $password . " " . $database . " > " . $backupPath . $backupFileName;
        
        system($command);

        header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.$backupFileName);
		readfile($backupPath.$backupFileName);
		unlink($backupPath.$backupFileName);
    }

}
