<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\StudentClass;
use App\Organisation;
use App\User;
use App\Quiz;
use Auth;
use DB;
use Excel;
use Session;

class ClassesController extends Controller {
	/**
	 * rules for the class form
	 */
	protected $rules = [
		'name' => 'required|min:3|max:40|unique:classes,name',
		'teacher_id' => 'required'
	];

	/**
	 * Display a listing of the classes.
	 *
	 * @return Response
	 */
	public function index()
	{
		$classes = Auth::user()->organisation->classesByPrivilege()->get();
		return view('classes.index',compact('classes'));
	}

	/**
	 * Show the form for creating a new class.
	 *
	 * @return Response
	 */
	public function create()
	{
		$teachers = Auth::user()->organisation
								->teachers()
								->get()
								->lists('name','id');
		return view('classes.create',compact('teachers'));
	}

	/**
	 * Store a newly created class in storage.
	 *
	 * @param  Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,$this->rules);

		$request['organisation_id'] = Auth::user()->organisation_id;

		$class = StudentClass::create($request->all());

		flash()->success($class->name.' has been created!');
		return redirect('classes');
	}

	/**
	 * Display the specified class detail.
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function show(StudentClass $class)
	{
		return view('classes.show',compact('class'));
	}

	/**
	 * Show the form for editing the specified class.
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function edit(StudentClass $class)
	{
		$teachers = Auth::user()->organisation
								->teachers()
								->get()
								->lists('name','id');
		return view('classes.edit',compact('teachers','class'));
	}

	/**
	 * Update the specified class in storage.
	 *
	 * @param  StudentClass  $class
	 * @param  Request $request
	 * @return Response
	 */
	public function update(StudentClass $class, Request $request)
	{
		$rules = [
			'name' => 'required|min:3|max:40|unique:classes,name,'.$class->id,
			'teacher_id' => 'required'
		];
		$this->validate($request,$rules);

		$class->update($request->all());
		flash()->success($class->name.'\'s detail has been updated!');
		return redirect()->route('classes.show',$class);
	}

	/**
	 * Remove the specified class from storage.
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function destroy(StudentClass $class)
	{
		$class->delete();
		flash()->success('You have successfully deleted a class!');
		return redirect('classes');
	}

	/**
	 * Show the form for allocating students to the specific class.
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function getStudents(StudentClass $class)
	{
		$exist_students = $class->students()->lists('id');
		$students = Auth::user()->organisation
								->students()
								->whereNotIn('id', $exist_students)
								->get()
								->lists('name','id');
		return view('classes.students',compact('class','students'));
	}

	/**
	 * Allocate students to the specific class
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function postStudents(StudentClass $class, Request $request)
	{
		$rules = [
			'student_list' => 'required',
		];
		$this->validate($request,$rules);

		$list = $request->input('student_list');
		foreach($list as $student) {
				$class->students()->attach($student);
		}
		flash()->success('You have successfully allocated students to this class!');
		return redirect()->route('classes.show-students',$class);
	}

	/**
	 * Remove students from a specific class
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function deleteStudent(StudentClass $class, Request $request)
	{
		$student = User::findOrFail($request->input('student_id'));
		$class->students()->detach($student);
		flash()->success('You have successfully removed student from this class!');
		return redirect()->route('classes.show-students',$class);
	}

	/**
	 * display form for class results
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function getClassResults(StudentClass $class, Request $request)
	{
		$quizzes = $class->students()
						->join('quiz_attempts','class_student.student_id','=','quiz_attempts.user_id')
						->join('quizzes','quizzes.id','=','quiz_attempts.quiz_id')
						->groupBy('quiz_attempts.quiz_id')->select('quizzes.*')->get()->lists('title','id');

		return view('classes.results',compact('class','quizzes'));
	}

	/**
	 * get class results for a specific class
	 *
	 * @param  StudentClass  $class
	 * @return array $results 
	 */
	public function postClassResults(StudentClass $class, Request $request)
	{
		if($request->ajax()) {
			$data = $request->all();
			$quiz_id = $request->input('quiz_id');
			$results = $class->students()
							->join('quiz_attempts','class_student.student_id','=','quiz_attempts.user_id')
							->join('quizzes','quizzes.id','=','quiz_attempts.quiz_id')
							->groupBy('quiz_attempts.quiz_id','class_student.student_id')
							->where('quizzes.id','=',$quiz_id)
							->select(DB::raw('count(*) as total_attempts, 
											(select count(*) from questions where questions.quiz_id = quizzes.id) as total_questions,
											max(score) as highest_score,
											min(score) as lowest_score,
											round(avg(score)) as average_score,
											users.id as user_id, 
											users.first_name, 
											users.last_name, quizzes.title as quiz_title'))
							->OrderBy('highest_score','DESC')
							->get();
			return $results;
  		}
    }

    /**
	 * display form for class results
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function getAllResults()
	{
		$quizzes = Auth::user()
					->organisation
    				->classes()
    				->select('quizzes.*')
					->join('class_student','classes.id','=','class_student.class_id')
					->join('users','users.id','=','class_student.student_id')
					->join('quiz_attempts','class_student.student_id','=','quiz_attempts.user_id')
					->join('quizzes','quizzes.id','=','quiz_attempts.quiz_id')
					->groupBy('quiz_attempts.quiz_id');

		if( Auth::user()->isTeacher() )
			$quizzes = $quizzes->where('classes.teacher_id','=',Auth::id())->get()->lists('title','id');
		else
			$quizzes = $quizzes->get()->lists('title','id');

		$classes = Auth::user()->organisation->classesByPrivilege()->lists('name', 'id');


		$quiz_id = $class_id = null;
		if( Session::has('quiz_id') ) {
			$quiz_id = Session::get('quiz_id');
		}
		if( Session::has('class_id') ) {
			$class_id = Session::get('class_id');
		}

		$results = $this->fetchStudentResults($class_id,$quiz_id);

		return view('classes.classes-results',compact('results','quizzes','classes','quiz_id','class_id'));
	}

	/**
	 * get class results for a specific class
	 *
	 * @param  StudentClass  $class
	 * @return array $results 
	 */
	public function postAllResults(Request $request)
	{
		$class_id = $request->input('class_id');
		$quiz_id = $request->input('quiz_id');
		if($request->input('filter_results')) {
			return redirect()
					->route('classes.show-classes-results')
					->with('class_id', $class_id)
					->with('quiz_id', $quiz_id);
		} else {
			$results = $this->fetchStudentResults($class_id,$quiz_id);

			Excel::create('MCFF-Classes-Results', function($excel) use($results) {
				$excel->setTitle('MCFF Classes Results');
				$excel->setCreator('MCFF')->setCompany('MCFF');
				$excel->setDescription('Class Results');

			    $excel->sheet('Results', function($sheet) use($results) {
					$sheet->freezeFirstRow();
			        $sheet->fromArray($results);
			    	$sheet->row(1, [ 
			    					'Quiz Title', 
			    					'Class Name', 
			    					'First Name',
			    					'Last Name', 
			    					'Total Attempts', 
			    					'Total Questions',
			    					'Highest Score',
			    					'Lowest Score',
			    					'Average Score'
			    					]);
			    });
			})->export('xls');
  		}
    }

	/**
	 * get classes results for a specific class by roles
	 *
	 * @return array $results 
	 */
    private function fetchStudentResults($class_id=null,$quiz_id=null){
    	$results = Auth::user()->organisation
    						->classes()
    						->select(DB::raw('
											quizzes.title as quiz_title, 
											classes.name as class_name,
											users.first_name, 
											users.last_name,
											(select count(*) from quiz_attempts where quiz_attempts.user_id = users.id AND quiz_attempts.quiz_id=quizzes.id) as total_attempts, 
											(select count(*) from questions where questions.quiz_id = quizzes.id) as total_questions,
											max(score) as highest_score,
											min(score) as lowest_score,
											round(avg(score)) as average_score
											'))
							->join('class_student','classes.id','=','class_student.class_id')
							->join('users','users.id','=','class_student.student_id')
							->join('quiz_attempts','class_student.student_id','=','quiz_attempts.user_id')
							->join('quizzes','quizzes.id','=','quiz_attempts.quiz_id')
							->groupBy('quiz_attempts.quiz_id','class_student.student_id','classes.id')
							
							->orderBy('classes.id')
							->orderBy('quizzes.id')
							->orderBy('users.id');

		if( !is_null($class_id) && !empty($class_id) ) 
			$results = $results->where('classes.id','=',$class_id);

		if( !is_null($quiz_id) && !empty($quiz_id) ) 
			$results = $results->where('quizzes.id','=',$quiz_id);

		if( Auth::user()->isTeacher() )
			return $results->where('classes.teacher_id','=',Auth::id())->get();
		else
			return $results->get();
    }
}
