<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Information;
use App\Country;
use Auth;
class InformationController extends Controller {
	/**
	 * rules for the class form
	 */
	protected $rules = [
		'information' => 'required|min:3',
		'country_code' => 'required'
	];

	/**
	 * Display a listing of the information.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notes = Auth::user()->organisation->information;
		//dd(Country::findOrFail('AUS')->information);
		return view('information.index',compact('notes'));
	}

	/**
	 * Show the form for creating a new information.
	 *
	 * @return Response
	 */
	public function create()
	{
		$exist_countries = Auth::user()->organisation->information->lists('country_code');
		$countries = Country::whereNotIn('id', $exist_countries)->orderBy('country_name')->lists('country_name','id');
		return view('information.create',compact('countries'));
	}

	/**
	 * Store a newly created information in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,$this->rules);

		$request['organisation_id'] = Auth::user()->organisation_id;

		$information = Information::create($request->all());

		flash()->success('Information for country <'.$information->country->country_name.'> has been stored!');
		return redirect('notes');
	}

	/**
	 * Display the specified information.
	 *
	 * @param  Information $information
	 * @return Response
	 */
	public function show(Information $information)
	{
		return view('information.show',compact('information'));
	}

	/**
	 * Show the form for editing the specified information.
	 *
	 * @param  Information  $information
	 * @return Response
	 */
	public function edit(Information $information)
	{
		$exist_countries = Auth::user()->organisation->information->where('country_code','!=',$information->country_code)->lists('country_code');
		$countries = Country::whereNotIn('id', $exist_countries)->orderBy('country_name')->lists('country_name','id');
		return view('information.edit',compact('countries','information'));
	}

	/**
	 * Update the specified information in storage.
	 *
	 * @param  Information $information
	 * @return Response
	 */
	public function update(Information $information,Request $request)
	{
		$this->validate($request,$this->rules);
		flash()->success('Information for country <'.$information->country->country_name.'> has been updated!');
		if($request->input('to_quiz')){
			//$information->update($request->except('to_quiz'));
			$information->update($request->all());
			return redirect()->route('user.country-quizzes',$information->country->iso2);
		}else{
			$information->update($request->all());
			return redirect('notes');
		}
	}

	/**
	 * Remove the specified information from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Information $information)
	{
		$information->delete();
		flash()->success('You have successfully deleted a country based note!');
		return redirect('notes');
	}

}
