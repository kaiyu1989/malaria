<?php namespace App\Http\Controllers;

use App\Quiz;
use App\Question;
use App\User;
use App\QuizAttempt;
use App\Country;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Auth;
use App\CountryReport;
/**
 * Visualisation Controller
 *
 * This controller renders your application's "visualisation" for users that are authenticated.
 * Specified to current user only
 *
 * @author     Kai Yu
 * @since      Delivery Cycle Three
 */
class VisualisationController extends Controller {

	/**
	 * get country data
	 *
	 * @param  Country  $country
	 * @return Response
	 */
	public function getCountryEstimateCases(Country $country)
	{
		return $country->countryReport()->orderBy('year')->get();
	}
}