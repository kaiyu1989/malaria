<?php namespace App\Http\Controllers;

use App\Quiz;
use App\Question;
use App\User;
use App\QuizAttempt;
use App\Country;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Auth;

/**
 * User Controller
 *
 * This controller renders your application's "user" for users that are authenticated.
 * Specified to current user only
 *
 * @author     Kai Yu
 * @since      Delivery Cycle One
 */
class UserController extends Controller {

	/**
	 * Index page, prevent user to access it, redirect to home.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return redirect('/');
	}

	/**
	 * Display current user profile page
	 *
	 * @return Response
	 */
	public function getProfile()
	{
		$user = Auth::user();
		return view('user.profile',compact('user'));
	}

	/**
	 * Show the form for editing current user profile.
	 *
	 * @return Response
	 */
	public function getProfileEdit(){
		$user = Auth::user();
		return view('user.profile-edit',compact('user'));
	}

	/**
	 * Update the current user in storage.
	 *
	 * @param UserRequest $request current request instance of the HTTP request with specific validation rules
	 * @return Response
	 */
	public function postProfileEdit(UserRequest $request)
	{
		Auth::user()->update($request->only('first_name','last_name','email','avatar'));
		flash()->success('Well done! You have updated your profile.');
		return redirect()->route('user.profile');
	}

	/**
	 * Show the form for change current user password.
	 *
	 * @return Response
	 */
	public function getProfilePassword(){
		$user = Auth::user();
		return view('user.profile-password',compact('user'));
	}

	/**
	 * Update the current user's password in storage.
	 *
	 * @param UserRequest $request current request instance of the HTTP request with specific validation rules
	 * @return Response
	 */
	public function postProfilePassword(UserRequest $request)
	{
		$request['password'] = bcrypt($request->input('new_password'));
		Auth::user()->update($request->only('password'));
		flash()->success('Well done! You have updated your password.');
		return redirect()->route('user.profile');
	}

	/**
	 * Display country based quizzes in map view
	 *
	 * @return Response
	 */
	public function getMap()
	{
		$quizzes = Quiz::latest()->get();
		return view('user.map',compact('quizzes'));
	}

	/**
	 * Display all quizzes in a list view
	 *
	 * @return Response
	 */
	public function getQuizzes()
	{
		$quizzes = Quiz::generalQuiz();
		return view('user.quizzes',compact('quizzes'));
	}

	/**
	 * Display a specific country with information and quizzes of the country
	 *
	 * @param string $iso2 iso standard 2 digits based country code
	 * @return Response
	 */
	public function getCountryQuizzes($iso2){
		$country = Country::where('iso2', '=', $iso2)->firstOrFail();
		return view('user.country-quizzes',compact('country'));
	}

	/**
	 * Display list of user attempts for a specific quiz
	 *
	 * @param int $quizId quiz id
	 * @return Response
	 */
	public function getQuizAttempts($quizId){
		$quizAttempts = Auth::user()->quizAttempts()->whereQuizId($quizId)->orderBy('created_at','desc')->get();
		$quiz = Quiz::findOrfail($quizId);
		return view('user.quiz-attempts',compact('quizAttempts','quiz'));
	}

	/**
	 * Display list of user attempts for a specific quiz
	 *
	 * @param int $quizId quiz id
	 * @return Response
	 */
	public function getQuizAttempt($quizId){
		$quiz = Quiz::findOrfail($quizId);
		return view('user.quiz-attempt',compact('quiz'));
	}

	/**
	 * Submit a new user attempt of a specific quiz
	 *
	 * @param int $quizId quiz id
	 * @return Response
	 */
	public function postQuizAttempt($quizId, Request $request){
		$score = 0;

		$quizAttempt = Auth::user()->quizAttempts()->create(['quiz_id'=>$quizId,'score'=>$score]);

		foreach($request->input('choices') as $question_id=>$choice_id) {
			Auth::user()->quizResponses()->create([
				'quiz_id'=>$quizId,
				'question_id'=>$question_id,
				'choice_id'=>$choice_id,
				'quiz_attempt_id'=>$quizAttempt->id
			]);
			$score = $this->isUserAnswerRight($question_id, $choice_id) ? $score+1 : $score;
		}

		$quizAttempt->update(['score'=>$score]);

		flash()->success('Well done! You have finished quiz.');
		return redirect()->route('user.quiz-attempt.review',$quizAttempt->id);
	}

	/**
	 * check current user's choice is the right anwser
	 *
	 * @param int $question_id question id
	 * @param int $choice_id  user choice id
	 * @return bool boolean to check user get the right answer or not
	 */
	private function isUserAnswerRight($question_id, $choice_id)
	{
		$question = Question::findOrfail($question_id);

		foreach($question->choices as $choice){
			if($choice_id == $choice->id && $choice->is_answer) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Display the review page of the quiz attempt
	 *
	 * @param int $quizAttemptId quiz attempt id
	 * @return Response
	 */
	public function getQuizReview($quizAttemptId)
	{
		$quizAttempt = QuizAttempt::findOrfail($quizAttemptId);
		$quiz = $quizAttempt->quiz;
		return view('user.quiz-review',compact('quizAttempt','quiz'));
	}
}
