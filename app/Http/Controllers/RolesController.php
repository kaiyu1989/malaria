<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Config;
use Route;
use App\User;
use App\QuizAttempt;
use Excel;
use Validator;
use Mail;
/**
 * Roles Controller
 *
 * This controller renders your application's "role" for users that are authenticated.
 * Specified list of users for roles
 *
 * @author     Kai Yu
 * @since      Delivery Cycle One
 */
class RolesController extends Controller {
	/**
	 * rules for the user form
	 */
	protected $rules = [
		'first_name' => 'required|max:255|alpha_space',
		'last_name' => 'required|max:255|alpha_space',
		'password' => 'required|min:6|confirmed',
		'email' => 'required|email|max:255|unique:users'
	];

	/**
	 * Index page, prevent user to access it, redirect to home.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return redirect('home');
	}

	/**
	 * Display list of staff
	 *
	 * @return Response
	 */
	public function getStaff()
	{
		$role = Config::get('malaria.role.staff');
		$users = Auth::user()->organisation->schools()->get();
		return view('roles.list-user',compact('users','role'));
	}

	/**
	 * Display list of teachers
	 *
	 * @return Response
	 */
	public function getTeacher()
	{
		$role = Config::get('malaria.role.teacher');
		$users = Auth::user()->organisation->teachers()->get();
		return view('roles.list-user',compact('users','role'));
	}

	/**
	 * Display list of students
	 *
	 * @return Response
	 */
	public function getStudent()
	{
		$role = Config::get('malaria.role.student');
		$users = Auth::user()->organisation->students()->get();
		return view('roles.list-user',compact('users','role'));
	}

	/**
	 * Display a specific user detail
	 *
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function getUser($user_id)
	{
		$user = User::findOrFail($user_id);
		
		if( !$this->hasPrivilege($user->role_id) ) {
			flash()->error('Sorry, you cannot view user with current priviledge!');
			return redirect('home');
		}

		$role_name = Config::get('malaria.role.roles.'.$user->role_id);

		return view('roles.view-user',compact('user','role_name'));
	}

	/**
	 * Display a view for create user
	 *
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function getCreateUser($role_name)
	{
		$role = Config::get('malaria.role.'.$role_name);
		
		if( !$this->hasPrivilege($role['id']) ) {
			flash()->error('Sorry, you cannot create user with current priviledge!');
			return redirect('home');
		}
		return view('roles.create-user',compact('role'));
	}

	/**
	 * Create user
	 *
	 * @param Request $request
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function postCreateUser($role_name,Request $request)
	{
		$this->validate($request,$this->rules);
		
		$role = Config::get('malaria.role.'.$role_name);

		$plain_password = $request->input('password');
		$request['organisation_id'] = Auth::user()->organisation_id;
		$request['password'] = bcrypt($request->input('password'));
		$request['role_id'] = $role['id'];
		
		$user = User::create($request->all());

		$user_arr = $user->toArray();
		$user_arr['plain_password'] = $plain_password;
		$user_arr['name'] = $user->name;

		Mail::send('emails.create-user', $user_arr, function($message) use ($user)
		{
			$message->subject('Your MCFF Account has been created');

			$message->to($user->email);
		});

		flash()->success($role['display_name'].' <'.$user->name.'> has been created! An email containing the login information has been sent to the registered email address.');
		return redirect()->route('roles.'.$role_name);
	}

	/**
	 * Display a view for edit user
	 *
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function getEditUser($user_id)
	{
		$user = User::findOrFail($user_id);
		
		if( !$this->hasPrivilege($user->role_id) ) {
			flash()->error('Sorry, you cannot edit user with current priviledge!');
			return redirect('home');
		}

		$role_name = Config::get('malaria.role.roles.'.$user->role_id);
		return view('roles.edit-user',compact('user','role_name'));
	}

	/**
	 * Update user detail
	 *
	 * @param Request $request
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function patchEditUser($user_id,Request $request)
	{
		$user = User::findOrFail($user_id);

		$rules = array_except($this->rules, array('password'));
		$rules['email'] = 'required|email|max:255|unique:users,email,'.$user->id;
		$this->validate($request,$rules);

		$user->update($request->all());

		flash()->success($user->role->name.' <'.$user->name.'> has been updated!');
		return redirect()->route('roles.show-user',$user_id);
	}

	/**
	 * Delete user
	 *
	 * @param Request $request
	 * @param $user_id user id to delete
	 * @return Response
	 */
	public function deleteUser($user_id, Request $request)
	{
		$user = User::findOrFail($user_id);

		if( !$this->hasPrivilege($user->role_id) ) {
			flash()->error('Sorry, you cannot delete user with current priviledge!');
			return redirect('home');
		}else{
			$user->delete();
			flash()->success('You have successfully deleted a '.$user->role->name.'!');
			return redirect()->route('roles.'.$request->role_name);
		}
	}

	/**
	 * Show the form for editing the specified user's password.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function getEditPassword($user_id)
	{
		$user = User::findOrFail($user_id);

		if( !$this->hasPrivilege($user->role_id) ) {
			flash()->error('Sorry, you cannot edit user\'s password with current priviledge!');
			return redirect('home');
		}

		$role_name = Config::get('malaria.role.roles.'.$user->role_id);
		return view('roles.edit-password',compact('user','role_name'));
	}

	/**
	 * Update the specified user's password in storage.
	 *
	 * @param Request $request
	 * @param  User $user
	 * @return Response
	 */
	public function patchEditPassword($user_id,Request $request)
	{
		$rules = array_only($this->rules, array('password'));
		$this->validate($request,$rules);

		$user = User::findOrFail($user_id);
		$new_password = $request->input('password');
		$request['password'] = bcrypt($request->input('password'));
		$user->update($request->all());

		Mail::send('emails.edit-password', ['password'=>$new_password], function($message) use ($user)
		{
			$message->subject('Your MCFF Password has been updated');

			$message->to($user->email);
		});

		flash()->success($user->role->name.' <'.$user->name.'>\'s password has been updated! New password has been sent to the registed email address.');
		return redirect()->route('roles.show-user',$user_id);
	}

	/**
	 * import students from csv or excel page
	 *
	 * @param Request $request
	 * @param $user_id user id
	 * @return Response
	 */
	public function getImportStudent()
	{
		return view('roles.import-user',compact('user','role_name'));
	}

	public function postImportStudent(Request $request)
	{
		$rules = [
			'file' => 'required|mimes:csv,xls,xlsx|max:5000'
		];
		$this->validate($request,$rules);

		$random_name = str_random(40);
		$path = base_path() . '/public/system/Organisation/'.Auth::user()->organisation_id.'/';
		$request->file('file')->move($path, $random_name);
		$users = Excel::load($path.$random_name, function($reader) {})->toArray();

		$errors = [];
		$users_succeed = 0;
		if($users){
			$user_rules = [
				'first_name' => 'required|max:255|alpha_space',
				'last_name' => 'required|max:255|alpha_space',
				'email' => 'required|email|max:255|unique:users'
			];
			foreach($users as $user){
				$v = Validator::make($user,$user_rules);

				if ($v->fails()) {
					$messages = null;
					foreach($v->errors()->all() as $message) {
						$messages .= $message;
					}
					$errors[] = $user['first_name'].'|'.$user['last_name'].'|'.$user['email'].' with errors: '.$messages;
				} else {
					$user['password'] = bcrypt('password');
					$user['organisation_id'] = Auth::user()->organisation_id;
					$user['role_id'] = Config::get('malaria.role.student_id');
					
					User::create($user);
					$users_succeed++;
				}
			}
		}
		if( count($errors)>0 ) {
			$flash_message = '<p>Total '.$users_succeed.' users have been created.</p>';
			$flash_message .= '<p>Following users are not loaded to the system.</p><ul>';
			foreach( $errors as $error) {
				$flash_message .= '<li>'.$error.'</li>'; 
			}
			$flash_message .= '</ul>';

			flash()->info($flash_message);	
			return redirect()->route('roles.show-import-student');
		} else {
			flash()->success('Students has been created!');	
			return redirect()->route('roles.student');
		}
	}

	/**
	 * user quizzes results
	 *
	 * @param Request $request
	 * @param $user_id user id
	 * @return Response
	 */
	public function getQuizResults($user_id)
	{
		$user = User::findOrFail($user_id);
		$role_name = Config::get('malaria.role.roles.'.$user->role_id);
		return view('roles.quiz-results',compact('user','role_name'));
	}

	/**
	 * user quiz attempt result
	 *
	 * @param $attempt_id attempt id
	 * @return Response
	 */
	public function getQuizAttempt($quizAttemptId)
	{
		$quizAttempt = QuizAttempt::findOrfail($quizAttemptId);
		$user = $quizAttempt->user;
		$quiz = $quizAttempt->quiz;
		$role_name = Config::get('malaria.role.roles.'.$user->role_id);
		return view('roles.quiz-attempt',compact('quizAttempt','quiz','role_name','user'));
	}

	/**
	 * check priviledge of current user
	 * cannot view user detail if the role greater than current user's priviledge
	 *
	 * @param $roleId role id of user to be actioned
	 * @return Response
	 */
	private function hasPrivilege($roleId)
	{
		if( $roleId <= Auth::user()->role_id ) {
			return false;
		}
		return true;
	}
}
