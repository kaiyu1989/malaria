<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Organisation;
use Auth;
use App\User;
use Config;
use Mail;
class SchoolsController extends Controller {
	/**
	 * rules for the school form
	 */
	protected $rules = [
		'name' => 'required|min:3|max:40|unique:organisations,name',
		'description' => 'required|min:3|max:255'
	];

	/**
	 * rules for the user form
	 */
	protected $user_rules = [
		'first_name' => 'required|min:3|max:255|alpha',
		'last_name' => 'required|min:3|max:255|alpha',
		'password' => 'required|min:6|confirmed',
		'email' => 'required|email|max:255|unique:users'
	];
	/**
	 * Display a listing of the schools.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schools = Organisation::all();
		return view('schools.index',compact('schools'));
	}

	/**
	 * Show the form for creating a new school.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schools.create');
	}

	/**
	 * Store a newly created school in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,$this->rules);

		$school = Organisation::create($request->all());

		flash()->success($school->name.' has been created!');
		return redirect('schools');
	}

	/**
	 * Display the specified school.
	 *
	 * @param  Organisation $school
	 * @return Response
	 */
	public function show(Organisation $school)
	{
		return view('schools.show',compact('school'));
	}

	/**
	 * Show the form for editing the specified school.
	 *
	 * @param  Organisation $school
	 * @return Response
	 */
	public function edit(Organisation $school)
	{
		return view('schools.edit',compact('school'));
	}

	/**
	 * Update the specified school in storage.
	 *
	 * @param  Organisation $school
	 * @param  Request $request
	 * @return Response
	 */
	public function update(Organisation $school, Request $request)
	{
		$rules = [
			'name' => 'required|min:3|max:40|unique:organisations,name,'.$school->id,
			'description' => 'required|min:3|max:255'
		];
		$this->validate($request,$rules);

		$school->update($request->all());
		flash()->success($school->name.'\'s detail has been updated!');
		return redirect()->route('schools.show',$school);
	}

	/**
	 * Remove the specified school from storage.
	 *
	 * @param  Organisation $school
	 * @return Response
	 */
	public function destroy(Organisation $school)
	{
		$school->delete();
		flash()->success('You have successfully deleted the school!');
		return redirect('schools');
	}

	/**
	 * Show school users
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function getUsers(Organisation $school)
	{
		return view('schools.users',compact('school'));
	}

	/**
	 * Display a view for create user
	 *
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function getCreateUser(Organisation $school)
	{
		if($school->admins->count()>0){
			flash()->error('The school has an admin already.');
			return redirect()->route('schools.users',$school);
		}
		return view('schools.create-admin',compact('school'));
	}

	/**
	 * Create user
	 *
	 * @param Request $request
	 * @param $role_name role name of the role
	 * @return Response
	 */
	public function postCreateUser(Organisation $school, Request $request)
	{
		$this->validate($request,$this->user_rules);

		$request['organisation_id'] = $school->id;
		$plain_password = $request->input('password');
		$request['password'] = bcrypt($request->input('password'));

		$request['role_id'] = Config::get('malaria.role.admin_id');
		
		$user = User::create($request->all());

		$user_arr = $user->toArray();
		$user_arr['plain_password'] = $plain_password;
		$user_arr['name'] = $user->name;

		Mail::send('emails.create-user', $user_arr, function($message) use ($user)
		{
			$message->subject('Your MCFF Account has been created');

			$message->to($user->email);
		});

		flash()->success('Admin <'.$user->name.'> has been created! An email containing the login information has been sent to the registered email address.');
		return redirect()->route('schools.users', $school);
	}


	/**
	 * Show school admin
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function getUser(Organisation $school, User $user)
	{
		//$user = User::findOrFail($user_id);
		return view('schools.show-user',compact('school','user'));
	}

	/**
	 * Show school users
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function getEditUser(Organisation $school, User $user)
	{
		if( !$user->isAdmin() ){
			flash()->error('You can only edit school admin.');
			return redirect()->route('schools.users',$school);
		}
		return view('schools.edit-user',compact('school','user'));
	}

	/**
	 * Edit school users
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function patchEditUser(Organisation $school, User $user, Request $request)
	{
		$rules = array_except($this->user_rules, array('password'));
		$rules['email'] = 'required|email|max:255|unique:users,email,'.$user->id;
		$this->validate($request,$rules);

		$user->update($request->all());

		flash()->success($user->role->name.' <'.$user->name.'> has been updated!');
		return redirect()->route('schools.show-user',[$school,$user]);
	}

	/**
	 * Show the form for editing the specified user's password.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function getEditPassword(Organisation $school, User $user)
	{
		if( !$user->isAdmin() ){
			flash()->error('You can only edit school admin.');
			return redirect()->route('schools.users',$school);
		}
		return view('schools.edit-password',compact('user','school'));
	}

	/**
	 * Update the specified user's password in storage.
	 *
	 * @param Request $request
	 * @param Organisation $school
	 * @param  User $user
	 * @return Response
	 */
	public function patchEditPassword(Organisation $school, User $user, Request $request)
	{
		$rules = array_only($this->user_rules, array('password'));
		$this->validate($request,$rules);
		$this->validate($request,$rules);

		$new_password = $request->input('password');
		$request['password'] = bcrypt($request->input('password'));
		$user->update($request->all());

		Mail::send('emails.edit-password', ['password'=>$new_password], function($message) use ($user)
		{
			$message->subject('Your MCFF Password has been updated');

			$message->to($user->email);
		});
		flash()->success($user->role->name.' < '.$user->name.' >\'s password has been updated! New password has been sent to the registed email address.');
		return redirect()->route('schools.show-user',[$school,$user]);
	}

	/**
	 * Show school users
	 *
	 * @param  StudentClass  $class
	 * @return Response
	 */
	public function deleteUser(Organisation $school, User $user)
	{
		$user->delete();
		flash()->success('You have successfully deleted user!');
		return redirect()->route('schools.users');
	}
}
