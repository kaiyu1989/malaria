<?php namespace App\Http\Controllers;

use App\Choice;
use App\Question;
use App\Quiz;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/**
 * Questions Controller
 *
 * This controller renders your application's "questions" for users that are authenticated.
 * @author     Kai Yu
 * @since      Delivery Cycle Two
 */
class QuestionsController extends Controller {

	/**
	 * rules for the question form
	 */
	protected $rules = [
		'question' => 'required|min:3|max:255'
	];

	/**
	 * index page, equivalent concept as quizzes.show page, redirect to it
	 *
	 * @return Response
	 */
	public function index(Quiz $quiz)
	{
		return redirect()->route('quizzes.show', [$quiz]);
		//return view('questions.index', compact('quiz'));
	}

	/**
	 * Show the form for creating a new question for a specific quiz.
	 *
	 * @param Quiz $quiz current quiz instance
	 * @return Response
	 */
	public function create(Quiz $quiz)
	{
		return view('questions.create', compact('quiz'));
	}

	/**
	 * Display the specified question within a quiz.
	 *
	 * @param  Quiz $quiz current quiz instance
	 * @param  Question $question current question instance
	 * @return Response
	 */
	public function show(Quiz $quiz, Question $question)
	{
		return view('questions.show',compact('quiz','question'));
	}

	/**
	 * Store a newly created question along with choices in storage.
	 * 
	 * @param Quiz $quiz current quiz instance
	 * @param Request $request current request instance of the HTTP request 
	 * @return Response
	 */
	public function store(Quiz $quiz, Request $request)
	{
		$this->validate($request,$this->rules);

		$inputQuestion = $request->input('question');

		$question = $quiz->questions()->create(['question'=>$inputQuestion]);

		$this->createChoices($request,$question);

		flash()->success('Question has been created!');
		return redirect()->route('quizzes.show', [$quiz]);
	}

	/**
	 * create choices for a question
	 * 
	 * @param Question $Question current quiz question instance
	 * @param Request $request current request instance of the HTTP request
	 */
	private function createChoices(Request $request, Question $question)
	{

		foreach($request->input('choices') as $index=>$choice) {
			$is_answer = ( $index == $request->input('answer') ) ? 1 : 0;
			$choice = $question->choices()->create( [ 'choice_name'=>$choice, 'is_answer'=>$is_answer ] );
		}
	}

	/**
	 * Show the form for editing the specified question.
	 *
	 * @param Question $Question current quiz question instance
	 * @param Quiz $quiz current quiz instance
	 * @return Response
	 */
	public function edit(Quiz $quiz, Question $question)
	{
		return view('questions.edit',compact('quiz','question'));
	}

	/**
	 * Update the specified question in storage.
	 *
	 * @param Question $Question current quiz question instance
	 * @param Quiz $quiz current quiz instance
	 * @param Request $request current request instance of the HTTP request
	 * @return Response
	 */
	public function update(Quiz $quiz , Question $question,Request $request)
	{
		$this->validate($request,$this->rules);

		$inputQuestion = $request->input('question');

		$question->update(['question'=>$inputQuestion]);

		$this->updateChoices($request,$question);

		flash()->success('Question : '.$question->question.' has been updated!');
		return redirect()->route('quizzes.questions.show', [$quiz,$question]);
	}

	/**
	 * Update choices for the question
	 *
	 * @param Request $request current request instance of the HTTP request
	 * @param Question $question current quiz question instance
	 */
	private function updateChoices(Request $request, Question $question) 
	{

		foreach($request->input('choices') as $id=>$choiceTxt) {
			$is_answer = ( $id == $request->input('answer') ) ? 1 : 0;
			if( strpos($id,'new_') !== false ) {
				$question->choices()->create( [ 'choice_name'=>$choiceTxt, 'is_answer'=>$is_answer ] );
			} else {
				$choice = Choice::findOrFail($id);
				$choice->update( [ 'choice_name'=>$choiceTxt, 'is_answer'=>$is_answer ] );
			}
		}
	}

	/**
	 * Remove the specified question from storage.
	 *
	 * @param Question $question current quiz question instance
	 * @param Quiz $quiz current quiz instance
	 * @return Response
	 */
	public function destroy(Quiz $quiz, Question $question)
	{
		$question->delete();
		flash()->success('You have successfully deleted a question!');
		return redirect()->route('quizzes.show', [$quiz]);
	}

	/**
	 * Remove question choice from storage.
	 *
	 * @param Question $question current quiz question instance
	 * @param Quiz $quiz current quiz instance
	 * @param int $choice_id choice id to delete 
	 * @return Response
	 */
	public function removeChoice(Quiz $quiz, Question $question,$choice_id)
	{
		$choice = Choice::findOrFail($choice_id);
		if( !$choice->is_answer ) {
			$choice->delete();
			flash()->success('You have successfully deleted an option!');
		} else {
			flash()->error('Sorry, you cannot delete the choice which is the answer!');
		}
		return redirect()->route('quizzes.questions.edit', [$quiz,$question]);
	}

}
