<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/benefit', 'WelcomeController@benefit');
Route::get('/screens', 'WelcomeController@screens');
Route::get('/support', 'WelcomeController@support');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

//manage schools
Route::group(['middleware' => ['auth','roles'],'roles' => ['super administrator']], function()
{

	Route::get('backup', 'HomeController@backup');
	Route::post('backup', 'HomeController@dumpBackUp');

	Route::resource('schools', 'SchoolsController');
	Route::get(
		'schools/{schools}/users',
		['uses'=>'SchoolsController@getUsers', 'as' => 'schools.users']
	);

	Route::get(
		'schools/{schools}/user/{users}',
		['uses'=>'SchoolsController@getUser', 'as' => 'schools.show-user']
	);

	//create user
	Route::get(
		'schools/{schools}/create-user',
		['uses'=>'SchoolsController@getCreateUser', 'as' => 'schools.create-user']
	);

	Route::post(
		'schools/{schools}/create-user',
		['uses'=>'SchoolsController@postCreateUser', 'as' => 'schools.store-user']
	);

	//edit user
	Route::get(
		'schools/{schools}/edit-user/{users}',
		['uses'=>'SchoolsController@getEditUser', 'as' => 'schools.edit-user']
	);

	Route::patch(
		'schools/{schools}/edit-user/{users}',
		['uses'=>'SchoolsController@patchEditUser', 'as' => 'schools.update-user']
	);

	//edit password
	Route::get(
		'schools/{schools}/edit-password/{users}',
		['uses'=>'SchoolsController@getEditPassword', 'as' => 'schools.edit-password']
	);

	Route::patch(
		'schools/{schools}/edit-password/{users}',
		['uses'=>'SchoolsController@patchEditPassword', 'as' => 'schools.update-password']
	);

	Route::delete(
		'schools/{schools}/user/{users}',
		['uses'=>'SchoolsController@deleteUser', 'as' => 'schools.destory-user']
	);
});

// manage users ----- this has been discarded
Route::group(['middleware' => ['auth','roles'],'roles' => ['administrator', 'school staff']], function()
{	
	Route::resource('users', 'UsersController');
	Route::get(
		'users/{users}/edit-password',
		['uses'=>'UsersController@editPassword', 'as' => 'users.edit-password']
	);

	Route::patch(
		'users/{users}/edit-password',
		['uses'=>'UsersController@updatePassword', 'as' => 'users.update-password']
	);
});

//view information of malaria, view own profile, quiz and attempts
Route::group(['middleware' => ['auth','roles'],'roles' => ['administrator', 'school staff', 'teacher', 'student']], function()
{

	Route::get('information', 'HomeController@information');
	
	Route::controller('user', 'UserController',[
	    'getQuizzes' => 'user.quizzes',
	    'getCountryQuizzes' => 'user.country-quizzes',
	    'getMap' => 'user.map',

	    'getQuizAttempts' => 'user.quiz-attempts',
	    'getQuizAttempt' => 'user.quiz-attempt.show',
	    'postQuizAttempt' => 'user.quiz-attempt.store',
	    'getQuizReview' => 'user.quiz-attempt.review',
	]);
});

//user profile module and ajax request
Route::group(['middleware' => ['auth']], function()
{
	Route::controller('user', 'UserController',[
	    'getIndex' => 'user.index',
	    'getProfile' => 'user.profile',

	    'getProfileEdit' => 'user.profile-edit',
	    'postProfileEdit' => 'user.profile-edit.update',

	    'getProfilePassword' => 'user.profile-password',
	    'postProfilePassword' => 'user.profile-password.update',
	]);

	Route::get(
		'visual/{country}/country-estimate-cases',
		['uses'=>'VisualisationController@getCountryEstimateCases', 'as' => 'visual.estimate-cases']
	);
});


// manage information
Route::group(['middleware' => ['auth','roles'],'roles' => ['school staff']], function()
{
	Route::resource('notes', 'InformationController');
});

//manage classes and quizzes
Route::group(['middleware' => ['auth','roles'],'roles' => ['school staff','teacher']], function()
{
	
	Route::resource('quizzes', 'QuizzesController');
	Route::resource('quizzes.questions', 'QuestionsController');
	Route::delete(
		'quizzes/{quizzes}/questions/{questions}/remove/{choices}',
		['uses'=>'QuestionsController@removeChoice', 'as' => 'quizzes.questions.remove']
	);

	Route::resource('classes', 'ClassesController');
	Route::get(
		'classes/{classes}/students',
		['uses'=>'ClassesController@getStudents', 'as' => 'classes.show-students']
	);

	Route::post(
		'classes/{classes}/students',
		['uses'=>'ClassesController@postStudents', 'as' => 'classes.allocate-students']
	);

	Route::delete(
		'classes/{classes}/students',
		['uses'=>'ClassesController@deleteStudent', 'as' => 'classes.delete-student']
	);

	Route::get(
		'classes/{classes}/class-results',
		['uses'=>'ClassesController@getClassResults', 'as' => 'classes.display-class-results']
	);

	Route::post(
		'classes/{classes}/class-results',
		['uses'=>'ClassesController@postClassResults', 'as' => 'classes.get-class-results']
	);

	Route::get(
		'classes-results',
		['uses'=>'ClassesController@getAllResults', 'as' => 'classes.show-classes-results']
	);

	Route::post(
		'classes-results',
		['uses'=>'ClassesController@postAllResults', 'as' => 'classes.fetch-classes-results']
	);
});


//manage users --roles controller replaced user controller
Route::group(['middleware' => ['auth','roles'],'roles' => ['administrator','school staff','teacher']], function()
{

	Route::controller('roles', 'RolesController',[
	    'getIndex' => 'roles.index',

	    'getStaff' => 'roles.staff',
	    'getTeacher' => 'roles.teacher',
	    'getStudent' => 'roles.student',
	    'getImportStudent' => 'roles.show-import-student',
	    'postImportStudent' => 'roles.create-import-student',

	    'getUser' => 'roles.show-user',
	    'getQuizResults' => 'roles.quiz-results',
	    'getQuizAttempt' => 'roles.quiz-attempt',

	    'getCreateUser' => 'roles.create-user',
	    'postCreateUser' => 'roles.store-user',

	    'getEditUser' => 'roles.edit-user',
	    'patchEditUser' => 'roles.update-user',

	    'getEditPassword' => 'roles.edit-password',
	    'patchEditPassword' => 'roles.update-password',

	    'deleteUser' => 'roles.destory-user',

	]);
});
//Route::get('user/profile',['uses' => 'UserController@showProfile', 'as' => 'user.profile']);