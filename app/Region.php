<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Region Model
 *
 * A table stores region information
 *
 * @author     Kai Yu
 * @since      Delivery Cycle One
 */
class Region extends Model {
	public $timestamps = false;
	/**
	 * A region can have many countries
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function countries() {
		return $this->hasMany('App\Country');
	}
}
