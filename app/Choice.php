<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Quiz choice Model
 *
 * A table stores quizzes choices along with the indicator to show right ones
 *
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class Choice extends Model {

	/**
	 * Fillable fields for a Choice
	 *
	 * @var array
	 */
	protected $fillable = [
		'choice_name',
		'is_answer'
	];

	public function scopeRandomOrder($query){
		$query->orderByRaw('RAND()');
	}

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A choice is owned by a question.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function question(){
		return $this->belongsTo('App\Question');
	}

	/**
	 * A choice can have many quiz responses
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizResponses(){
		return $this->hasMany('App\QuizResponse');
	}
}
