<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Carbon\Carbon;

use Config;
/**
 * User Model
 *
 * A user  table stores user information
 *
 * @author     Laravel Framework
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, StaplerableInterface {

	use Authenticatable, CanResetPassword, EloquentTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'email', 'password', 'avatar','organisation_id','role_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * Additional Field to treat this as Carbon instances
	 *
	 * @var array
	 */
	protected $dates = ['avatar_updated_at'];

	protected $have_role;
	/**
	 * Constructor of User model, configuration of attached files
	 *
	 */
	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
			'default_url' => '/img/malaria/default-user-icon-profile.png'
        ]);

        parent::__construct($attributes);
    }

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

    /**
	 * Set the avatar_updated_at attribute
	 * set_____Attribute convention for mutator
	 *
	 * @var $date
	 */
	public function setAvatarUpdatedAtAttribute($date) {
		$this->attributes['avatar_updated_at'] =Carbon::parse($date)->timestamp;
	}

	/**
	 * get the avatar_updated_at attribute
	 * get_____Attribute convention for accessor
	 *
	 * @var $date
	 */
	public function getAvatarUpdatedAtAttribute($date) {
		return Carbon::createFromTimeStamp($date)->toDateTimeString();
	}

	/**
	 * get full name of user
	 * get_____Attribute convention for accessor
	 *
	 * @var $date
	 */
	public function getNameAttribute() {
		return ucfirst($this->attributes['first_name']) . ' ' . ucfirst($this->attributes['last_name']);
	}

	/**
	 * A user can have many articles
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizzes(){
		return $this->hasMany('App\Quiz');
	}

	/**
	 * A user can have many quiz responses
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizResponses(){
		return $this->hasMany('App\QuizResponse');
	}

	/**
	 * A user can have many quiz attempts
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizAttempts(){
		return $this->hasMany('App\QuizAttempt');
	}

	/**
	 * A user is owned by an organisation.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function organisation() {
		return $this->belongsTo('App\Organisation');
	}

	/**
	 * a student belongs to multiple classes
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function classes() {
		return $this->belongsToMany('App\StudentClass','class_student','student_id','class_id');
	}

	/**
	 * A user can have one role
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function role()
	{
		return $this->hasOne('App\Role','id', 'role_id');
	}

	/**
	 * check if user has roles available in the list
	 * 
	 * @param array role list
	 * @return bool if user has role
	 */
	public function hasRole($roles)
	{
		$this->have_role = $this->getUserRole();
		if(is_array($roles)){
			foreach($roles as $need_role){
				if($this->checkIfUserHasRole($need_role)) {
					return true;
				}
			}
		} else{
			return $this->checkIfUserHasRole($roles);
		}
		return false;
	}

	/**
	 * fetch user roles
	 * 
	 * @return array user roles
	 */
	private function getUserRole()
	{
		return $this->role()->getResults();
	}

	/**
	 * check if user has specific roles
	 * 
	 * @return array user roles
	 */
	private function checkIfUserHasRole($need_role)
	{
		return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
	}

	/**
	 * check if user is a super admin
	 * 
	 * @return bool is a super admin
	 */
	public function isSuperAdmin()
	{
		return ($this->role_id==Config::get('malaria.role.super_id')) ? true : false;
	}

	/**
	 * check if user is an admin
	 * 
	 * @return bool is an admin
	 */
	public function isAdmin()
	{
		return ($this->role_id==Config::get('malaria.role.admin_id')) ? true : false;
	}

	/**
	 * check if user is a school role
	 * 
	 * @return bool is a school
	 */
	public function isSchool()
	{
		return ($this->role_id==Config::get('malaria.role.staff_id')) ? true : false;
	}

		/**
	 * check if user is a teacher
	 * 
	 * @return bool is a teacher
	 */
	public function isTeacher()
	{
		return ($this->role_id==Config::get('malaria.role.teacher_id')) ? true : false;
	}

	/**
	 * check if user is a student
	 * 
	 * @return bool is a student
	 */
	public function isStudent()
	{
		return ($this->role_id==Config::get('malaria.role.student_id')) ? true : false;
	}
}
