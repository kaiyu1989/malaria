<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Quiz score Model
 *
 * A user quiz table stores user quizzes attempts with their scores
 *
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class QuizAttempt extends Model {

	/**
	 * Fillable fields for a QuizScore
	 *
	 * @var array
	 */
	protected $fillable = [
		'quiz_id',
		'user_id',
		'score'
	];

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A quiz score is owned by a quiz.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function quiz(){
		return $this->belongsTo('App\Quiz');
	}

	/**
	 * A quiz score is owned by a user.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
		return $this->belongsTo('App\User');
	}

	/**
	 * A quiz score can have many quiz responses
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizResponses(){
		return $this->hasMany('App\QuizResponse');
	}
}
