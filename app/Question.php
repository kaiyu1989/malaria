<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Question Model
 *
 * A table stores quizzes questions
 *
 * @author     Kai Yu
 * @since      Delivery Cycle one
 */
class Question extends Model {

	/**
	 * Fillable fields for a Quiz
	 *
	 * @var array
	 */
	protected $fillable = [
		'quiz_id',
		'question'
	];

	public function scopeRandomOrder($query){
		$query->orderByRaw('RAND()');
	}

	/**
	 * Customize date format, timestamp formate to unix timestamp
	 *
	 */
	protected function getDateFormat()
    {
        return 'U';
    }

	/**
	 * A question is owned by a quiz.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function quiz(){
		return $this->belongsTo('App\Quiz');
	}

	/**
	 * A question can have many choices
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function choices(){
		return $this->hasMany('App\Choice');
	}

	/**
	 * A question can have many quiz responses
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizResponses(){
		return $this->hasMany('App\QuizResponse');
	}
}
