<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Country Model
 *
 * A table stores country information
 *
 * @author     Kai Yu
 * @since      Delivery Cycle One
 */
class Country extends Model {
	public $timestamps = false;

	/**
	 * Fillable fields for country
	 *
	 * @var array
	 */
	protected $fillable = [
		'iso2'
	];
	
	/**
	 * A user can have many articles
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quizzes() {
		return $this->hasMany('App\Quiz','country_code','id');
	}

	/**
	 * A country can have many information
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function information() {
		return $this->hasMany('App\Information','country_code','id');
	}

	/**
	 * A user can have many articles
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function countryReport() {
		return $this->hasMany('App\CountryReport','country_code','id');
	}

	/**
	 * A country is owned by a region.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function region() {
		return $this->belongsTo('App\Region','region_code','id');
	}

	public function scopeCountryOfTheDay($query){
		return $query->join('quizzes','countries.id','=','quizzes.country_code')->whereNotNull('quizzes.id')->orderByRaw('RAND()')->first();
	}
}
