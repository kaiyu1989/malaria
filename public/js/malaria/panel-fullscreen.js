$(".panel-fullscreen").on("click",function(){
    panel_fullscreen($(this).parents(".panel"));
    return false;
});


function panel_fullscreen(panel){    
    if(panel.hasClass("panel-fullscreened")){
        panel.removeClass("panel-fullscreened").unwrap();
        panel.css("height","");
        panel.find(".panel-body>div").css("height","");
        panel.find(".panel-fullscreen .fa").removeClass("fa-compress").addClass("fa-expand");
        $(window).resize();
    }else{
        var head    = panel.find(".panel-heading");
        var body    = panel.find(".panel-body");
        var footer  = panel.find(".panel-footer");
        var hplus   = 30;
        
        if(body.hasClass("panel-body-table") || body.hasClass("padding-0")){
            hplus = 0;
        }
        if(head.length > 0){
            hplus += head.height();
        } 
        if(footer.length > 0){
            hplus += footer.height();
        } 
        panel.height($(window).height());
        panel.find('.panel-body>div').height($(window).height()-hplus);
        //panel.find(".panel-body,.chart-holder").css({ "max-height": '' });
        
        
        panel.addClass("panel-fullscreened").wrap('<div class="panel-fullscreen-wrap"></div>');        
        panel.find(".panel-fullscreen .fa").removeClass("fa-expand").addClass("fa-compress");
        
        $(window).resize();
    }
}