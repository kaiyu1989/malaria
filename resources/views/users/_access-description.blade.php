<ul style="margin-top: 2px;" class="panel-controls-left" id="system-feature">
	<li>
		<span class="glyphicon glyphicon-ok"></span><span class="text">Quiz Map</span>
	</li>
	<li>
		<span class="glyphicon glyphicon-ok"></span><span class="text">Attempt Quiz</span>
	</li>
	<li>
		<span class="glyphicon glyphicon-ok"></span><span class="text">Profile Management</span>
	</li>
	<li>
		<span class="glyphicon glyphicon-ok"></span><span class="text">Own Avatar</span>
	</li>

	<li class="advance-feature">
		<span class="glyphicon glyphicon-ok"></span><span class="text">User Management</span>
	</li>
	<li class="advance-feature">
		<span class="glyphicon glyphicon-ok"></span><span class="text">Ouiz Builder</span>
	</li>
	<li class="advance-feature">
		<span class="glyphicon glyphicon-ok"></span><span class="text">Malaria Notes</span>
	</li>
</ul>