@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('users_edit',$user) !!}
	</div>

	<div class="col-md-4">
		@include('users._menu',['user'=>$user,'has_feature'=>false])
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Edit User - {{$user->name}}</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($user,['method'=>'PATCH','route'=>['users.update-password',$user],'class'=>'form-horizontal']) !!}
					@include('users._form',['submitButtonText'=>'Update Password','allow_password'=>true])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop