<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<span class="fa fa-user"></span>
				<a href="{{route('users.show',[$user])}}">{{$user->name}} Detail</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-edit"></span>
				<a href="{{route('users.edit',[$user])}}">Edit General Information</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-lock"></span>
				<a href="{{route('users.edit-password',[$user->id])}}">Edit Password</a>
			</li>
		</ul>
	</div>
</div>


@if(isset($has_feature)&&$has_feature)
<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">User Features</h3>
	</div>
	
	<div class="panel-body">
        @include('users._access-description')
	</div>
</div>


@section('footer')
<script>
if($('#role_list').val() != 4) {
	$('.advance-feature').show();
} else {
	$('.advance-feature').hide();
}

$('#role_list').on('change', function() {
	if( $(this).val() != 4 ){
		$('.advance-feature').show();
	} else {
		$('.advance-feature').hide();
	}
});


roleID = '{{$user->role_id}}';
if(roleID != 4) {
	$('.advance-feature').show();
} else {
	$('.advance-feature').hide();
}
</script>
@stop
@endif