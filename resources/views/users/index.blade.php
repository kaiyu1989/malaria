@extends('app')

@section('content')
{!! Breadcrumbs::render('users') !!}
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Users </h3>
			<div class="pull-right">
               <a href="{{route('users.create')}}" class="btn btn-success">Create a User</a>
            </div>
		</div>
		
		<div class="panel-body">
			@if(!$users->count())
			<div class="alert alert-info">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        There is no user yet.
		    </div>
			@else
            <div class="table-responsive" id="ulist">
				<table class="table">
					<thead>
						<tr>
							<th><span>User</span></th>
							<th><span>Classes</span></th>
							<th><span>Role</span></th>
							<th><span>Email</span></th>
							<th><span>Created at</span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr class="user-info" user-id="{{$user->id}}">
							<td>
								<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail user-avatar">
								<a href="{{ route('users.show', [$user->id] ) }}" class="ulink">{{$user->name}}</a>
							</td>
							<td>
								@foreach ( $user->classes as $class)
								<p>
									<a href="{{ route('classes.show', [$class] ) }}">
									{{$class->name}}
									</a>
								</p>
								@endforeach
							</td>
							<td>
								<span class="label label-primary">{{$user->role->name}}</span>
							</td>
							<td>
								{{$user->email}}
							</td>
							<td>{{$user->created_at->diffForHumans()}}</td>
							<td style="width: 20%;">
							@if($user->id != Auth::user()->id)
								<a href="{{ route( 'users.edit',[$user] ) }}" class="table-link">
									<span class="fa-stack fa-lg">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="#" class="table-link text-danger delete-user">
									<span class="fa-stack fa-lg">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif
		</div>
	</div>
@stop

@section('modal')
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete User</h4>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<p>Are you sure you want to delete user <strong id="d-name"></strong>?</p>
					<div></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'url' => ['users']]) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
    <script>
    $('.delete-user').on('click',function(e) {
        e.preventDefault();
        $('#d-name').html($(this).closest('.user-info').find('td:nth-child(1)').html());
        userId = $(this).closest('.user-info').attr('user-id');
        console.log($('#delete-form').attr("action"));
        $('#delete-form').attr("action",'/users/'+userId)
        $('#deleteModal').modal();
    });
    </script>
@stop