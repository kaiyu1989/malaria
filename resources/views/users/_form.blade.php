@if(isset($allow_general)&&$allow_general)
<div class="form-group">
	{!! Form::label('first_name','First Name*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::text('first_name',null,['class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('last_name','Last Name*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::text('last_name',null,['class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('email','Email*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::text('email',null,['class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('role_id','Role*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::select('role_id',$roles,isset($role)?$role:null,['id'=>'role_list','class'=>'form-control']) !!}
	</div>
</div>
@endif

@if(isset($allow_password)&&$allow_password)
<div class="form-group">
	{!! Form::label('password','Password*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::password('password',['class'=>'form-control','autocomplete'=>'off']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('password_confirmation','Password Confirmation*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::password('password_confirmation',['class'=>'form-control']) !!}
	</div>
</div>
@endif

<div class="form-group">
	<div class="col-sm-offset-4 col-sm-6">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary'])!!}
	</div>
</div>