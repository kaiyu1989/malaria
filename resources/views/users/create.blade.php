@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('users_create') !!}
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Create a new User</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($user = new \App\User, ['url'=>'/users','class'=>'form-horizontal']) !!}
					@include('users._form',['submitButtonText'=>'Create User','allow_password'=>true,'allow_general'=>true])
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">User Features</h3>
			</div>
			
			<div class="panel-body">
                @include('users._access-description',['hasAdvanceFeature'=>true])
			</div>
		</div>
	</div>
@stop



@section('footer')
<script>
if($('#role_list').val() != 4) {
	$('.advance-feature').show();
} else {
	$('.advance-feature').hide();
}

$('#role_list').on('change', function() {
	if( $(this).val() != 4 ){
		$('.advance-feature').show();
	} else {
		$('.advance-feature').hide();
	}
});

</script>
@stop