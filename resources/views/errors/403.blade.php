@extends('app')

@section('content')
    
	<div class="col-md-12">
		<div class="panel panel-info panel-colorful">
			<div class="panel-heading">
				<h5>Whoops, looks like you are not authorized.</h5>
			</div>

			<div class="panel-body">
			    <h4>Malaria is still there though. You can always make donation to stop malaria.</h4>
			    <a href="https://www.againstmalaria.com/Donation.aspx" class="btn btn-success" target="_blank">Donate now</a>
			</div>
		</div>
	</div>
@stop