@extends('app')

@section('content')
<div class="col-md-8">
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Dashboard</h3>
		</div>

		<div class="panel-body">
			@if( Auth::user()->isSuperAdmin() )
				@include('home._super-admin')
			@elseif( Auth::user()->isAdmin() )
				@include('home._admin')
			@elseif( Auth::user()->isSchool() )
				@include('home._staff')
			@elseif( Auth::user()->isTeacher() )
				@include('home._teacher')
			@elseif( Auth::user()->isStudent() )
				@include('home._student')
			@endif
		</div>
	</div>
</div>
<div class="col-md-4">
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Quick Link</h3>
		</div>

		<div class="panel-body">
			<ul style="font-size:18px;">
				@if (Auth::user()->isSuperAdmin())
				<li><a href="{{ route('schools.create') }}">Add a School</a></li>
				@endif
				
				@if (Auth::user()->isAdmin())
				<li><a href="{{ route('roles.create-user','staff') }}">Add a School Staff</a></li>
				@endif

				@if (Auth::user()->isAdmin() || Auth::user()->isSchool())
				<li><a href="{{ route('roles.create-user','teacher') }}">Add a Teacher</a></li>
				@endif

				@if (Auth::user()->isAdmin() || Auth::user()->isSchool() || Auth::user()->isTeacher())
				<li><a href="{{ route('roles.create-user','student') }}">Add a Student</a></li>
				@endif

				@if (Auth::user()->isSchool())
				<li><a href="{{ route('notes.create') }}">Add Country Information</a></li>
				@endif

				@if (Auth::user()->isSchool())
				<li><a href="{{ route('quizzes.create') }}">Add a Quiz</a></li>
				@endif

				@if (Auth::user()->isSchool()||Auth::user()->isTeacher())
				<li><a href="{{ route('classes.show-classes-results') }}">Class Results</a></li>
				@endif

				@if (Auth::user()->isStudent())
					<li><a href="{{ route('user.profile-password') }}">Change Password</a></li>
					@if(!$country->get()->isEmpty())
					<li><a href="{{ route('user.country-quizzes',[$country->iso2]) }}">Country of the Day : {{$country->country_name}}</a></li>
					@endif
				@endif
			</ul>
		</div>	
    </div>
</div>
@endsection
