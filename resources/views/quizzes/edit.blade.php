@extends('app')

@section('content')
	{!! Breadcrumbs::render('quiz_edit',$quiz) !!}

	<div class="col-md-12 well">
		<h1>Edit:{{$quiz->title}}</h1>
		<hr/>

		@include('errors.list')
		{!! Form::model($quiz,['method'=>'PATCH','action'=>['QuizzesController@update',$quiz->id]]) !!}
			@include('quizzes._form',['submitButtonText'=>'Update Quiz'])
		{!! Form::close() !!}
	</div>
@stop