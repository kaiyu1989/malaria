@extends('app')

@section('content')
    {!! Breadcrumbs::render('quizzes') !!}
    <div class="col-md-12 well">
        <h1>
            Quizzes
            @if( !Auth::user()->isTeacher() )
            <a href="{{url('/quizzes/create')}}" class="btn btn-info">Create a Quiz</a>
            @endif
        </h1>

        <hr/>

        @if ( !$quizzes->count() )
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            You have no quiz.
        </div>
        @else
            @foreach($quizzes as $quiz)
            <article>
                <h2>
                    <a href="{{url('/quizzes',$quiz->id)}}">

                        <span class="badge badge-inverse" style="font-size:20px;">{{$quiz->questions->count()}}</span>
                        @if($quiz->country)
                            {{$quiz->country->country_name}}
                        @endif
                        Quiz : {{$quiz->title}}
                    </a>
                    @if( !Auth::user()->isTeacher() )
                    <a href="{{ route( 'quizzes.edit',[$quiz->id] ) }}" class="btn btn-primary">Edit</a>
                    @endif
                </h2>
                <div class="body"></div>
            </article>  

            @endforeach
        @endif
    </div>
@stop