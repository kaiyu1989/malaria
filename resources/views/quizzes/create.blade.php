@extends('app')

@section('content')
	{!! Breadcrumbs::render('quiz_create') !!}
	
	<div class="col-md-12 well">
		<h2>Write a New Quiz</h2>
		<hr/>

		@include('errors.list')
		{!! Form::model($quiz = new \App\Quiz, ['url'=>'/quizzes']) !!}
			@include('quizzes._form',['submitButtonText'=>'Create Quiz'])
		{!! Form::close() !!}
	</div>
@stop