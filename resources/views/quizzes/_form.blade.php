<div class="form-group">
	{!! Form::label('title','Title:')!!}
	{!! Form::text('title',null,['class'=>'form-control'])!!}
</div>

<div class="form-group">
	<div class="row">
		<div class="col-md-3">
		{!! Form::label('country_list','Is this a country based quiz?') !!}
		</div>
		<div class="col-md-6">
		@if($quiz->country_code)
			{!! Form::checkbox('is_general',1,true,['id'=>'is_general']) !!}
		@else
			{!! Form::checkbox('is_general',0,false,['id'=>'is_general']) !!}
		@endif
		</div>
	</div>
	<!--{!! Form::select('country_code',array(''=>'General') + $countries,null,['id'=>'country_list','class'=>'form-control']) !!}-->
	{!! Form::select('country_code',$countries,null,['id'=>'country_list','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control'])!!}
</div>

@section('footer')
	<script>
	$(function() {
		if( (country_code = '{{$quiz->country_code}}') ) {
			$('#country_list').show();
			$('#country_list').val('{{$quiz->country_code}}');
		} else {
			$('#country_list').hide();
		}

		$("[name='is_general']").bootstrapSwitch();
	});

	$('#is_general').on('switchChange.bootstrapSwitch', function(event, state) {
		//console.log(this); // DOM element
		//console.log(event); // jQuery event
		//console.log(state); // true | false

		if( state) {
			$('#country_list').show();
			if( (country_code = '{{$quiz->country_code}}') ) {
				$('#country_list').val('{{$quiz->country_code}}');
			} else {
				$('#country_list').val($("#country_list option:first").val());
			}
		} else {
			$('#country_list').hide();
		}
	});

	/*$('#is_general').on('click',function() {
		if( $(this).is(':checked') ) {
			$('#country_list').show();
		} else {
			$('#country_list').hide();
		}
	});*/
	</script>
@stop