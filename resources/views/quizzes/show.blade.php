@extends('app')

@section('content')
    {!! Breadcrumbs::render('quiz',$quiz) !!}
    <div class="col-md-12 well">
    <h1>
        @if($quiz->country){{$quiz->country->country_name}}@endif Quiz : {{$quiz->title}} 

        @if( !Auth::user()->isTeacher() )
        <div class="btn-group">
            <a href="{{ action('QuizzesController@edit',[$quiz->id]) }}" class="btn btn-primary">Edit Quiz</a>
            <a href="{{ route( 'quizzes.questions.create',[$quiz->id] ) }}" class="btn btn-primary">Add a question</a>

            <a class="btn btn-danger-malaria btn-danger">Delete this Quiz</a>
        </div>
        @endif
    </h1>

    <ul class="list-unstyled">
        <li>Total {{$quiz->questions->count()}} questions</li>
        <li>Updated {{$quiz->updated_at->diffForHumans() }} </li>
    </ul>

    <hr/>

    @if( !$quiz->questions->count() )
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            This quiz does not have any questions yet.
        </div>
    @else
        @foreach( $quiz->questions as $question )
        <div class="row">
            <div class="col-md-12">
                <h3>
                    @if( !Auth::user()->isTeacher() )
                    <a href="{{ route( 'quizzes.questions.show',[$quiz->id,$question->id] ) }}">{{$question->question}}</a>
                    <a href="{{ route( 'quizzes.questions.edit',[$quiz->id,$question->id] ) }}" class="btn btn-primary">Edit</a>
                    @else
                    {{$question->question}}
                    @endif
                </h3>
            </div>
            
            <div class="col-md-12">
                <ul class="list-group">
                    @foreach( $question->choices as $choice )
                        @if( $choice->is_answer )
                            <li class="list-group-item list-group-item-success">{{$choice->choice_name}}</li>
                        @else
                            <li class="list-group-item">{{$choice->choice_name}}</li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
<!--
        <article>
            <h3>
                <a href="{{route('quizzes.questions.show',[$quiz->id,$question->id])}}">{{$question->question}}</a>
            </h3>
            <div class="body">
                <ul class="list-group">
                    @foreach( $question->choices as $choice )
                        @if( $choice->is_answer )
                            <li class="list-group-item list-group-item-success">{{$choice->choice_name}}</li>
                        @else
                            <li class="list-group-item">{{$choice->choice_name}}</li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </article>
-->
        @endforeach
    @endif
    </div>
@stop

@section('modal')
    <div id="deleteModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete Choice</h4>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <p>Are you sure you want to delete this quiz - <strong id="modal-choice">{{$quiz->title}}</strong> with total {{$quiz->questions->count()}} question(s)?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'route' => ['quizzes.destroy',$quiz->id]] ) !!}
                        
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            <button class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script>
    $('.btn-danger-malaria').on('click',function(e) {
        e.preventDefault();
        $('#deleteModal').modal();
    });
    </script>
@stop