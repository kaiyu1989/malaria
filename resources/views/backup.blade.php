@extends('app')

@section('content')
<div class="col-md-8">
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Backup Database</h3>
		</div>

		<div class="panel-body">
			{!! Form::open([ 'url'=>'/backup' ]) !!}
	        <div class="form-group">
				{!! Form::submit("Click to backup",['class'=>'btn btn-primary'])!!}
			</div>
	    	{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
