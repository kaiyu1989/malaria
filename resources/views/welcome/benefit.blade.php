<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

@include('partials.header')

<body style="background:#FFFFFF !important;">
   
    <header>
        @include('partials.nav')
    </header>

    <section id="features">
        <div class="container">
            <div class="section-heading scrollpoint sp-effect3">
                <h1>Benefits</h1>
                <div class="divider"></div>
                <p>Learn more about MCFF</p>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 scrollpoint sp-effect1">
                    <div class="media media-left feature">
                        <i class="fa fa-book fa-2x pull-right"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Improves Learning</h3>
                        </div>
                    </div>
                    <div class="media media-left feature">
                        <i class="fa fa-lightbulb-o fa-2x pull-right"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Interactive</h3>
                        </div>
                    </div>
                    <div class="media media-left feature">
                        <i class="fa fa-lightbulb-o fa-2x pull-right"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Visually Interesting</h3>
                            
                        </div>
                    </div>
                    <div class="media media-left feature">
                        <i class="fa fa-child fa-2x pull-right"></i>
                    
                        <div class="media-body">
                            <h3 class="media-heading">Easy to use</h3>
                        </div>
                    </div>
                    <div class="media media-left feature">
                        <i class="fa fa-flash fa-2x pull-right"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading pull-right">Fast learning process</h3>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4" >
                    <img src="img/malaria/lightbulb.png" class="img-responsive scrollpoint sp-effect5" alt="">
                </div>
                <div class="col-md-4 col-sm-4 scrollpoint sp-effect2">
                    <div class="media feature">
                        <i class="fa fa-thumbs-o-up fa-2x pull-left"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Assisted Teaching Material</h3>
                        </div>
                    </div>
                    <div class="media feature">
                        <i class="fa fa-check fa-2x pull-left"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Grades and Scores Reporting</h3>
                        </div>
                    </div>
                    <div class="media feature">
                        <i class="fa fa-question-circle fa-2x pull-left"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Flexible quizzes</h3>
                            
                        </div>
                    </div>
                    <div class="media feature">
                        <i class="fa fa-laptop fa-2x pull-left"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">Portable</h3>
                            
                        </div>
                    </div>
                    <div class="media feature">
                        <i class="fa fa-plus fa-2x pull-left"></i>
                        
                        <div class="media-body">
                            <h3 class="media-heading">And much more!</h3>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('partials.footer')

    {!! Html::script('js/jquery-1.11.1.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/slick.min.js') !!}
    {!! Html::script('js/placeholdem.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}
    {!! Html::script('js/waypoints.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
    <script>
        $(document).ready(function() {


            appMaster.animateScript();

        });
    </script>
</body>

</html>
