<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

@include('partials.header')

<body style="background:#FFFFFF !important;">
    <header>
        @include('partials.nav')
    </header>
<section id="support" class="doublediagonal">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Support</h1>
                    <div class="divider"></div>
                    <p>For more info and support, contact us!</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 scrollpoint sp-effect1">
                                <form role="form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Your email">
                                    </div>
                                    <div class="form-group">
                                        <textarea cols="30" rows="10" class="form-control" placeholder="Your message"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-4 contact-details scrollpoint sp-effect2">
                                <div class="media">
                                    <i class="fa fa-map-marker fa-2x pull-left"></i>
                                    <div class="media-body">
                                        <h4 class="media-heading">Caulfield Campus, Monash University, Australia</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <i class="fa fa-envelope fa-2x pull-left"></i>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="mailto:support@monashmalaria.com">support@monashmalaria.com</a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <i class="fa fa-phone fa-2x pull-left"></i>
                                    <div class="media-body">
                                        <h4 class="media-heading">+61 03 0000 0000</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    @include('partials.footer')

    {!! Html::script('js/jquery-1.11.1.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/slick.min.js') !!}
    {!! Html::script('js/placeholdem.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}
    {!! Html::script('js/waypoints.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
    <script>
        $(document).ready(function() {


            appMaster.animateScript();


        });
    </script>
</body>

</html>
