<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

@include('partials.header')

<style>
.slick-slide{
    width: 550px !important;
    margin-right: 15px;
    margin-left: 15px;
}

/* Makes images fully responsive */

.img-responsive,
.thumbnail > img,
.thumbnail a > img,
.carousel-inner > .item > img,
.carousel-inner > .item > a > img {
  display: block;
  width: 100%;
  height: auto;
}

/* ------------------- Carousel Styling ------------------- */

.carousel-inner {
  border-radius: 15px;
}

.carousel-caption {
  background-color: rgba(0,0,0,.5);
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  padding: 0 0 10px 25px;
  color: #fff;
  text-align: left;
}

.carousel-indicators {
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  width: 100%;
  z-index: 15;
  margin: 0;
  padding: 0 25px 25px 0;
  text-align: right;
}

.carousel-control.left,
.carousel-control.right {
  background-image: none;
}


/* ------------------- Section Styling - Not needed for carousel styling ------------------- */

.section-white {
   padding: 10px 0;
}

.section-white {
  background-color: #fff !important;
  color: #555;
}

.section-white h2{
    color:#FFF;
}

@media screen and (min-width: 768px) {

  .section-white {
     padding: 1.5em 0;
  }

}

/*@media screen and (min-width: 992px) {

  .container {
    max-width: 930px;
  }

}*/
</style>

<body style="background:#FFFFFF !important;">
   
    <header>
        @include('partials.nav')
    </header>


    <section id="screens" class="section-white">
        <div class="container">

            <div class="section-heading scrollpoint sp-effect3" style="margin-bottom:0px;">
                <h1>Sneak peek of MCFF</h1>
                <div class="divider"></div>
                <p>Check out the features of MCFF</p>
            </div>

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/1.png">
                            <img src="img/malaria/screenshots/1.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Dashboard</h2>
                        </div>
                    </div>
                    
                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/2.png">
                            <img src="img/malaria/screenshots/2.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Manage Teachers</h2>
                        </div>
                    </div>

                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/3.png">
                            <img src="img/malaria/screenshots/3.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Import Students CSV</h2>
                        </div>
                    </div>

                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/4.png">
                            <img src="img/malaria/screenshots/4.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Manage Students</h2>
                        </div>
                    </div>

                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/5.png">
                            <img src="img/malaria/screenshots/5.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Classes</h2>
                        </div>
                    </div>

                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/6.png">
                            <img src="img/malaria/screenshots/6.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Class Information</h2>
                        </div>
                    </div>

                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/7.png">
                            <img src="img/malaria/screenshots/7.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Class Results</h2>
                        </div>
                    </div>

                    <div class="item">
                        <a class="fancybox" rel="group" href="img/malaria/screenshots/8.png">
                            <img src="img/malaria/screenshots/8.png" alt="" height="375" width="500">
                        </a>
                        <div class="carousel-caption">
                            <h2>Malaria Information</h2>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </section>

    @include('partials.footer')

    {!! Html::script('js/jquery-1.11.1.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/slick.min.js') !!}
    {!! Html::script('js/placeholdem.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}
    {!! Html::script('js/waypoints.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
    {!! Html::script('fancybox/jquery.fancybox.pack.js') !!}
    <script>
        $(document).ready(function() {
            
            //appMaster.screensCarousel();

            appMaster.animateScript();


            $(".fancybox").fancybox();
        });
    </script>
</body>

</html>