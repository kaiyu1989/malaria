<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

@include('partials.header')

<body style="background:#FFFFFF !important;">

    <div class="pre-loader">
        <div class="load-con">
            <img src="/img/malaria/logo.png" class="animated fadeInDown" alt="">
            <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
        </div>
    </div>
   
    <header>
        @include('partials.nav')
    </header>

    <section id="about">
    	<div class="container">

    		<div class="section-heading scrollpoint sp-effect3">
    			<h1>About Us</h1>
    			<div class="divider"></div>
    			<p>Why Malaria-Cure From Future?</p>
    		</div>

    		<div class="row">
    			<div class="col-md-3 col-sm-3 col-xs-6">
    				<div class="about-item scrollpoint sp-effect2">
    					<i class="fa fa-child fa-2x"></i>
    					<h3>Start From Young</h3>
    					<p>To train kids from young and to be aware of Malaria at young age</p>
    				</div>
    			</div>
    			<div class="col-md-3 col-sm-3 col-xs-6" >
    				<div class="about-item scrollpoint sp-effect5">
    					<i class="fa fa-briefcase fa-2x"></i>
    					<h3>Learning Material</h3>
    					<p>Malaria to be made as a learning material so that students can understand better</p>
    				</div>
    			</div>
    			<div class="col-md-3 col-sm-3 col-xs-6" >
    				<div class="about-item scrollpoint sp-effect5">
    					<i class="fa fa-users fa-2x"></i>
    					<h3>Community</h3>
    					<p>Aims to form a community that is aware of the severity of Malaria</p>
    				</div>
    			</div>
    			<div class="col-md-3 col-sm-3 col-xs-6" >
    				<div class="about-item scrollpoint sp-effect1">
    					<i class="fa fa-heart fa-2x"></i>
    					<h3>Help the needy ones</h3>
    					<p>Help the affected ones by spreading the knowledge</p>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    @include('partials.footer')

    {!! Html::script('js/jquery-1.11.1.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/slick.min.js') !!}
    {!! Html::script('js/placeholdem.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}
    {!! Html::script('js/waypoints.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
    <script>
        $(document).ready(function() {


            appMaster.animateScript();


            appMaster.preLoader();

        });
    </script>
</body>

</html>
