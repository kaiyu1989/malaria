@extends('app')

@section('content')

	
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Information</h3>
			<div class="pull-right">
               <a href="{{route('notes.create')}}" class="btn btn-success">Create information</a>
            </div>
		</div>
		
		<div class="panel-body">
			@if(!$notes->count())
				<div class="alert alert-info">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        No Information is available.
			    </div>
			@else
            <div class="table-responsive" id="ulist">
			<table class="table">
				<thead>
					<tr>
						<th><span>Information</span></th>
						<th><span>Country</span></th>
						<th><span>Created at</span></th>
						<th>&nbsp;</th>
					</tr>
				</thead>

				<tbody>
				@foreach($notes as $note)
					<tr class="note-info" note-id="{{$note->id}}">
						<td>
							<a href="{{ route('notes.show', [$note->id] ) }}">{!!substr(nl2br($note->information),0,50).'...'!!}</a>
						</td>
						<td>
							{{$note->country->country_name}}
						</td>
						<td>{{$note->created_at->diffForHumans()}}</td>
						<td style="width: 20%;">
							<a href="{{ route( 'notes.edit',[$note] ) }}" class="table-link">
								<span class="fa-stack fa-lg">
									<i class="fa fa-square fa-stack-2x"></i>
									<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
								</span>
							</a>
							<a href="#" class="table-link text-danger delete-user">
								<span class="fa-stack fa-lg">
									<i class="fa fa-square fa-stack-2x"></i>
									<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
								</span>
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			</div>
			@endif
		</div>
	</div>
@stop

@section('modal')
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Information</h4>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<p>Are you sure you want to delete information <strong id="d-name"></strong>?</p>
					<div></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'url' => ['classes']]) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
    <script>
    $('.delete-user').on('click',function(e) {
        e.preventDefault();
        $('#d-name').html($(this).closest('.note-info').find('td:nth-child(1)').html());
        infoId = $(this).closest('.note-info').attr('note-id');
        $('#delete-form').attr("action",'/notes/'+infoId)
        $('#deleteModal').modal();
    });
    </script>
@stop