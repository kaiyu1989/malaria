@extends('app')

@section('content')

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Create information</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($class = new \App\Information, ['url'=>'/notes','class'=>'form-horizontal']) !!}
					@include('information._form',['submitButtonText'=>'Create Information'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop