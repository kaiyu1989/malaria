@extends('app')

@section('content')
	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Information</h3>
			</div>
			
			<div class="panel-body">
				<p><strong>{{$information->country->country_name}}</strong></p>
				<p>{!!nl2br($information->information)!!}</p>
			</div>
		</div>
	</div>
@stop