@extends('app')

@section('content')
	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Edit Information</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($information,['method'=>'PATCH','route'=>['notes.update',$information],'class'=>'form-horizontal']) !!}
					@include('information._form',['submitButtonText'=>'Update Information'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop