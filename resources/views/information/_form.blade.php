<div class="form-group">
	{!! Form::label('country_code','Country*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::select('country_code',$countries,null,['id'=>'country_code','class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('name','Information*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::textarea('information',null,['class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-4 col-sm-6">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary'])!!}
	</div>
</div>