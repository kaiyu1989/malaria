@extends('app')

@section('content')
    <div class="col-md-12">
		{!! Breadcrumbs::render('roles_user_detail',$role_name,$user) !!}
	</div>

	<div class="col-md-3">
		@include('roles._user-menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">{{$user->role->name}} Information</h3>
				<!--<div class="pull-right">
					<a class="btn btn-success" href="{{route('users.edit', [$user])}}">
						<span class="fa fa-user"></span> Edit User
					</a>
				</div>-->
			</div>
			
			<div class="panel-body">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="text-center">
						<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail">
					</div>
				</div>
				<!-- edit form column -->
				<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
					<h2>{{$user->name}}</h2>
					<p><strong>Email: </strong> {{$user->email}} </p>
					@if( $user->isStudent() )
					<p>
						<strong>Class: </strong>
						@foreach($user->classes as $class)
						<span class="label label-default">{{$class->name}}</span>
						@endforeach
					</p>
					@endif
					<p>Joined since {{$user->created_at->diffForHumans()}}</p>

					@if( $user->isStudent() )
					<div class="col-xs-12 col-sm-4 emphasis">
						<h2><strong> {{$user->quizAttempts->groupBy('quiz_id')->count()}} </strong></h2>
						<p><small>Quizzes</small></p>
						<!--<button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Quizzes </button>-->
					</div>

					<div class="col-xs-12 col-sm-4 emphasis">
						<h2><strong> {{$user->quizAttempts->count()}} </strong></h2>                 
						<p><small>Quiz Attempts</small></p>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@stop