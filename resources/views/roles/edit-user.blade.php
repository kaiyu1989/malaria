@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('roles_user_edit',$role_name,$user) !!}
	</div>

	<div class="col-md-3">
		@include('roles._user-menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Edit {{$user->role->name}} - {{$user->name}}</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($user,['method'=>'PATCH','route'=>['roles.update-user',$user],'class'=>'form-horizontal']) !!}
					@include('roles._form-user',['submitButtonText'=>'Update Information','allow_general'=>true])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop