<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon-list-alt"></span>
				<a href="{{route('roles.'.$role_name)}}">Back to {{$user->role->name}} List</a>
			</li>
		</ul>

		<ul class="list-group">
			<li class="list-group-item">
				<span class="fa fa-user"></span>
				<a href="{{route('roles.show-user',[$user])}}">{{$user->name}}'s Detail</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-edit"></span>
				<a href="{{route('roles.edit-user',[$user])}}">Edit General Information</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-lock"></span>
				<a href="{{route('roles.edit-password',[$user->id])}}">Edit Password</a>
			</li>
			@if( $user->isStudent() )
			<li class="list-group-item">
				<span class="fa fa-graduation-cap"></span>
				<a href="{{route('roles.quiz-results',[$user->id])}}">Quiz Results</a>
			</li>
			@endif
		</ul>
	</div>
</div>