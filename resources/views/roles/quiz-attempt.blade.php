@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('roles_user_quiz_attempts',$role_name,$user,$quizAttempt) !!}
	</div>

    <div class="col-md-3">
		@include('roles._user-menu')
	</div>

    <div class="col-md-9">
    	<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">
					Review {{$user->name}}'s @if($quiz->country){{$quiz->country->country_name}}@endif Quiz : {{$quiz->title}}
				</h3>
				<div class="pull-right">
					 <a href="{{ route( 'roles.quiz-results',$user ) }}" class="btn btn-primary">Finish Review</a>
				</div>
			</div>
			
			<div class="panel-body">
				<ul class="list-unstyled">
		            <li>{{$quiz->questions->count()}} questions in total</li>
		            <li>
		            	The score is {{$quizAttempt->score}}/{{$quiz->questions->count()}}
		            </li>
		        </ul>

		        @if( !$quiz->questions->count() )
		        <div class="alert alert-info">
		        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        	Nothing can be reviewed.
		        </div>
		        @else
			        @foreach( $quizAttempt->quizResponses as $response)
			        <div class="row">
			        	<div class="col-md-12">
			        		<h3>
			        			{{$response->question->question}}
			        		</h3>
			        	</div>
			        	<div class="col-md-12">
			        		<ul class="list-group">
		        			@foreach( $response->question->choices as $choice )
			        			@if( $choice->is_answer )                    
			        			<li class="list-group-item list-group-item-success">
			        				<span class="label label-success">Right Answer</span>
			        				@if( $response->choice_id == $choice->id )
			        				<span class="label label-success">Your Answer</span>
			        				@endif
			        			@elseif( ($response->choice_id == $choice->id) && !$choice->is_answer )
			        			<li class="list-group-item list-group-item-danger">
			        				<span class="label label-danger">Your Answer</span>
		        				@else
		        				<li class="list-group-item">
		        				@endif
		        					{{$choice->choice_name}}
		        				</li>
		        			@endforeach
		        			</ul>
			        	</div>
			        </div>
			        @endforeach
			        
			        <a href="{{ route( 'roles.quiz-results',$user ) }}" class="btn btn-primary">Finish Review</a>
		        @endif
			</div>
		</div>
	</div>
@stop