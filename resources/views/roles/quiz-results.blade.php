@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('roles_user_quiz_results',$role_name,$user) !!}
	</div>

	<div class="col-md-3">
		@include('roles._user-menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">{{$user->name}}'s Quiz Results</h3>
			</div>
			
			<div class="panel-body">
				@if(!$user->quizAttempts->groupBy('quiz_id')->count())
				<div class="alert alert-info">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{$user->name}} has not taken any quiz yet.
			    </div>
				@else
	            <div class="table-responsive">
					@foreach($user->quizAttempts->groupBy('quiz_id') as $quiz_id=>$attempts)
						<table class="table">
						<thead>
							<tr>
								<th><span>Quiz</span></th>
								<th><span>#Attempt</span></th>
								<th><span>Score</span></th>
								<th><span>Attempted At</span></th>
							</tr>
						</thead>
						<tbody>
								<?php $max_score = 0;?>
								@foreach($attempts as $index=>$attempt)
									<?php
										if($attempt->score > $max_score) {
											$max_score = $attempt->score;
											$max_score_id = $attempt->id;
										}

									?>
								@endforeach
								@foreach( $attempts as $index=>$attempt)
								<?php 
									if($max_score_id==$attempt->id)
										echo "<tr class='info'>";
									else
										echo "<tr>";
								?>
									<td width="40%">{{$attempt->quiz->title}}</td>
									<td>
										<a href="{{route('roles.quiz-attempt',$attempt->id)}}">#{{$index+1}}</a>
									</td>
									<td>{{$attempt->score}}/{{$attempt->quiz->questions->count()}}</td>
									<td>{{$attempt->created_at->diffForHumans()}}</td>
								</tr>
								@endforeach
								<tr>
									<td colspan="4"></td>
									<!--<td colspan="2"><strong class="pull-right">Highest Score</strong></td>
									<td colspan="2">
										{{$user->quizAttempts()->whereQuizId($quiz_id)->orderBy('score','desc')->first()->score}}/{{end($attempts)->quiz->questions->count()}}</td>-->
								</tr>

						</tbody>
					</table>
					@endforeach
				</div>
				@endif
			</div>
		</div>
	</div>
@stop