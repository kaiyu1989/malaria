<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			@if (!Auth::user()->isTeacher())
			@if (!Auth::user()->isSchool())
			<li class="list-group-item">
				<span class="fa fa-group"></span>
				<a href="{{route('roles.staff')}}">School Staff</a>
			</li>
			@endif
			<li class="list-group-item">
				<span class="fa fa-group"></span>
				<a href="{{route('roles.teacher')}}">Teachers</a>
			</li>
			@endif
			<li class="list-group-item">
				<span class="fa fa-group"></span>
				<a href="{{route('roles.student')}}">Students</a>
			</li>
		</ul>

		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon-import"></span>
				<a href="{{route('roles.show-import-student')}}">Import Students</a>
			</li>
		</ul>
	</div>
</div>