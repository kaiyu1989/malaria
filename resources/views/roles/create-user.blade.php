@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('roles_user_create',$role['link']) !!}
	</div>

	<div class="col-md-3">
		@include('roles._menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Create a new {{$role['display_name']}}</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($user = new \App\User, ['route'=>['roles.store-user',$role['link']],'class'=>'form-horizontal']) !!}
					@include('roles._form-user',['submitButtonText'=>'Create '.$role['display_name'],'allow_password'=>true,'allow_general'=>true])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop