@extends('app')

@section('content')
	<div class="col-md-12">
	</div>
	
	<div class="col-md-3">
		@include('roles._menu')
	</div>

	<div class="col-md-9">
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Import students from excel with xls,csv format</h3>
		</div>
		
		<div class="panel-body">
			<div class="alert alert-info">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        Please upload file with extension <span class="label label-success">.xls</span>
		        <span class="label label-success">.xlsx</span>
		        <span class="label label-success">.csv</span>.
		        <p>File format should follow as: </p>
		        <p>First Name | Last Name | Email </p>
		    </div>

		    @include('errors.list')
			{!! Form::open([ 'route'=>['roles.create-import-student'],'method'=>'POST','files' => true]) !!}
			 	
				{!! Form::file('file',['class'=>'text-center center-block well well-sm']) !!}

				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-6">
					{!! Form::submit('Import Users',['class'=>'btn btn-primary'])!!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	</div>
@stop