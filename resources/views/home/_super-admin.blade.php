<div class="col-md-4">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-university fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{$schools->count()}} schools</h3>
				</div>
			</div>
		</div>
		<a href="{{route('schools.index')}}">
			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-6">View</div>
					<div class="col-xs-6 text-right">
						<i class="fa fa-arrow-circle-right"></i>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>