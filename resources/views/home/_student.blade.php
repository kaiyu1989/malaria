<div class="col-md-6">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-certificate fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->organisation->quizzes->count()}} available quizzes</h3>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-check fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->quizAttempts->groupBy('quiz_id')->count()}} finished quizzes</h3>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-thumbs-o-up fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->quizAttempts->count()}} attempts in total</h3>
				</div>
			</div>
		</div>
	</div>
</div>