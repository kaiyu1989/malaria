<div class="col-md-4">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-users fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->organisation->teachers->count()}} teachers</h3>
				</div>
			</div>
		</div>
		<a href="{{route('roles.teacher')}}">
			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-6">View</div>
					<div class="col-xs-6 text-right">
						<i class="fa fa-arrow-circle-right"></i>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>

<div class="col-md-4">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-users fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->organisation->students->count()}} students</h3>
				</div>
			</div>
		</div>
		<a href="{{route('roles.student')}}">
			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-6">View</div>
					<div class="col-xs-6 text-right">
						<i class="fa fa-arrow-circle-right"></i>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>

<div class="col-md-4">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-certificate fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->organisation->quizzes->count()}} quizzes</h3>
				</div>
			</div>
		</div>
		<a href="{{route('quizzes.index')}}">
			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-6">View</div>
					<div class="col-xs-6 text-right">
						<i class="fa fa-arrow-circle-right"></i>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>

<div class="col-md-4">
	<div class="panel panel-info ">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-4">
					<i class="fa fa-graduation-cap fa-4x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<h3>{{Auth::user()->organisation->classes->count()}} classes</h3>
				</div>
			</div>
		</div>
		<a href="{{route('classes.index')}}">
			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-6">View</div>
					<div class="col-xs-6 text-right">
						<i class="fa fa-arrow-circle-right"></i>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>