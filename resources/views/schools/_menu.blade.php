<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon-info-sign"></span>
				<a href="{{route('schools.show',[$school])}}">School Detail</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-edit"></span>
				<a href="{{route('schools.edit',[$school])}}">Edit School</a>
			</li>
			<li class="list-group-item">
				<span class="fa fa-group"></span>
				<a href="{{route('schools.users',[$school])}}">School Users</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-duplicate"></span>
				<a href="{{route('schools.create-user',[$school])}}">Add an Admin</a>
			</li>
		</ul>
	</div>
</div>