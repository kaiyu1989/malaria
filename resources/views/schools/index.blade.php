@extends('app')

@section('content')
{!! Breadcrumbs::render('classes') !!}
	<div class="panel panel-default panel-colorful">
		<div class="panel-heading">
			<h3 class="panel-title">Schools</h3>
			<div class="pull-right">
               <a href="{{route('schools.create')}}" class="btn btn-success">Add a School</a>
            </div>
		</div>
		
		<div class="panel-body">
			@if(!$schools->count())
			<div class="alert alert-info">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    	There is no school yet.
		    </div>
			@else
            <div class="table-responsive" id="ulist">
				<table class="table">
					<thead>
						<tr>
							<th><span>School Name</span></th>
							<th><span>Description</span></th>
							<th><span>Created at</span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>

					<tbody>
					@foreach($schools as $school)
						<tr class="school-info" school-id="{{$school->id}}">
							<td>
								<a href="{{ route('schools.show', [$school->id] ) }}" class="school-link">{{$school->name}}</a>
							</td>
							<td>{{$school->description}}</td>
							<td>{{$school->created_at->diffForHumans()}}</td>
							<td style="width: 20%;">
								<a href="{{ route( 'schools.edit',$school ) }}" class="table-link">
									<span class="fa-stack fa-lg">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="#" class="table-link text-danger delete-school">
									<span class="fa-stack fa-lg">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			@endif
		</div>
	</div>
@stop

@section('modal')
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Class</h4>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<p>Are you sure you want to delete school <strong id="d-name"></strong>?</p>
					<div></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'url' => ['schools']]) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
    <script>
    $('.delete-school').on('click',function(e) {
        e.preventDefault();
        $('#d-name').html($(this).closest('.school-info').find('td:nth-child(1)').html());
        schoolId = $(this).closest('.school-info').attr('school-id');
        $('#delete-form').attr("action",'/schools/'+schoolId)
        $('#deleteModal').modal();
    });
    </script>
@stop