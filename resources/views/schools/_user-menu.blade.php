<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon-list-alt"></span>
				<a href="{{route('schools.users',$school)}}">Back to User List</a>
			</li>
		</ul>

		<ul class="list-group">
			<li class="list-group-item">
				<span class="fa fa-user"></span>
				<a href="{{route('schools.show-user',[$school,$user])}}">{{$user->name}}'s Detail</a>
			</li>
			@if( $user->isAdmin() )
			<li class="list-group-item">
				<span class="glyphicon glyphicon-edit"></span>
				<a href="{{route('schools.edit-user',[$school,$user])}}">Edit General Information</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-lock"></span>
				<a href="{{route('schools.edit-password',[$school,$user])}}">Edit Password</a>
			</li>
			@endif
		</ul>
	</div>
</div>