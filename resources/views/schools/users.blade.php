@extends('app')

@section('content')
	<div class="col-md-12">
	</div>

	<div class="col-md-4">
		@include('schools._menu')
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">School users</h3>
			</div>
			
			<div class="panel-body">
				<div class="table-responsive" id="ulist">
					<table class="table">
						<thead>
							<tr>
								<th><span>User</span></th>
								<th><span>Role</span></th>
								<th><span>Email</span></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($school->users()->orderBy('role_id')->get() as $user)
							<tr class="user-info" user-id="{{$user->id}}">
								<td>
									<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail user-avatar">
									<a href="{{ route('schools.show-user', [$school,$user] ) }}" class="ulink">{{$user->name}}</a>
								</td>
								<td>
									<span class="label label-primary">{{$user->role->name}}</span>
								</td>
								<td>
									{{$user->email}}
								</td>
								<td style="width: 20%;">
									@if($user->isAdmin())
									<a href="{{ route( 'schools.edit-user',[$school,$user]) }}" class="table-link">
										<span class="fa-stack fa-lg">
											<i class="fa fa-square fa-stack-2x"></i>
											<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
										</span>
									</a>
									<a href="#" class="table-link text-danger delete-school">
										<span class="fa-stack fa-lg">
											<i class="fa fa-square fa-stack-2x"></i>
											<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
										</span>
									</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('modal')
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete User</h4>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<p>Are you sure you want to delete user <strong id="d-name"></strong>?</p>
					<div></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'route' => ['schools.destory-user',$school]]) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
    <script>
    $('.delete-school').on('click',function(e) {
        e.preventDefault();
        $('#d-name').html($(this).closest('.user-info').find('td:nth-child(1)').html());
        schoolId = $(this).closest('.user-info').attr('user-id');
        action = $('#delete-form').attr("action");
        $('#delete-form').attr("action",action+'/'+schoolId)
        $('#deleteModal').modal();
    });
    </script>
@stop