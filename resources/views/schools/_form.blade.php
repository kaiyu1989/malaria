<div class="form-group">
	{!! Form::label('name','School Name*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::text('name',null,['class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('description','Description',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::textarea('description',null,['class'=>'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-4 col-sm-6">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary'])!!}
	</div>
</div>