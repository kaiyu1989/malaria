@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('users_create') !!}
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Create a new School</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($organisation = new \App\Organisation, ['url'=>'/schools','class'=>'form-horizontal']) !!}
					@include('schools._form',['submitButtonText'=>'Add a new School'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="col-md-4">
		@include('schools._help')
	</div>
@stop