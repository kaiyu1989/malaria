<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Tips</h3>
	</div>
	
	<div class="panel-body">
                <div class="col-md-12">
                        <span class="glyphicon glyphicon-info-sign col-md-1"></span>		
        	       <span class="text col-md-10">Each school has one admin only.</span>
                </div>
                <div class="col-md-12">
                        <span class="glyphicon glyphicon-info-sign col-md-1"></span>
                	<span class="text col-md-10">Super Administrator manages the school.</span>
                </div>
                <div class="col-md-12">
                        <span class="glyphicon glyphicon-info-sign col-md-1"></span>
                        <span class="text col-md-10">Super Administrator manage school admin account. Other type of roles are view only.</span>
                </div>
	</div>
</div>