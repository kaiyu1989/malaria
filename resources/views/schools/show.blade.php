@extends('app')

@section('content')
    <div class="col-md-12">

	</div>

	<div class="col-md-4">
		@include('schools._menu')
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">School Information</h3>
			</div>
			
			<div class="panel-body">
				<!-- edit form column -->
				<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
					<table class="table">
					<tr>
						<td>Name: </td>
						<td>{{$school->name}}</td>
					</tr>
					<tr>
						<td>Description: </td>
						<td>{{$school->description}}</td>
					</tr>
					<tr>
						<td>Total users: </td>
						<td>{{$school->users->count()}}</td>
					</tr>
					<tr>
						<td>Total classes: </td>
						<td>{{$school->classes->count()}}</td>
					</tr>
					<tr>
						<td>Created at: </td>
						<td>{{$school->created_at->diffForHumans()}}</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop