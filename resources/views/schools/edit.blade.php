@extends('app')

@section('content')
	<div class="col-md-12">
	</div>

	<div class="col-md-4">
		@include('schools._menu')
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Edit School Detail</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($school,['method'=>'PATCH','route'=>['schools.update',$school],'class'=>'form-horizontal']) !!}
					@include('schools._form',['submitButtonText'=>'Update School Detail'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop