@extends('app')

@section('content')
	<div class="col-md-12">
	</div>

	<div class="col-md-3">
		@include('schools._menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Create a new Admin</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($user = new \App\User, ['route'=>['schools.store-user',$school],'class'=>'form-horizontal']) !!}
					@include('roles._form-user',['submitButtonText'=>'Create Admin','allow_password'=>true,'allow_general'=>true])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop