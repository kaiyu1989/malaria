@extends('app')

@section('content')
    <div class="col-md-12">
	</div>

	<div class="col-md-3">
		@include('schools._user-menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">{{$user->role->name}} Information</h3>
				<!--<div class="pull-right">
					<a class="btn btn-success" href="{{route('users.edit', [$user])}}">
						<span class="fa fa-user"></span> Edit User
					</a>
				</div>-->
			</div>
			
			<div class="panel-body">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="text-center">
						<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail">
					</div>
				</div>
				<!-- edit form column -->
				<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
					<h2>{{$user->name}}</h2>
					<p><strong>Email: </strong> {{$user->email}} </p>
					<p>Joined since {{$user->created_at->diffForHumans()}}</p>
				</div>
			</div>
		</div>
	</div>
@stop