<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

@include('partials.header')

<body>
   
    <header>
        @include('partials.nav')
    </header>


    <div class="wrapper">
        
        <section>
             <div class="container">
                    
                <div class="section-heading scrollpoint sp-effect3"></div>
                <div class="row">@include('flash::message')</div>
                <div class="row">@yield('content')</div>
            </div>
        </section>

        @include('partials.footer')

    </div>
    @yield('modal')



    {!! Html::script('js/jquery-1.11.1.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/slick.min.js') !!}
    {!! Html::script('js/placeholdem.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}
    {!! Html::script('js/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}
    {!! Html::script('js/waypoints.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
    {!! Html::script('js/jquery-jvectormap-2.0.2.min.js') !!}
    {!! Html::script('js/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('js/jquery-jvectormap-asia-mill-en.js') !!}
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('/js/bootstrap-switch.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-switch.js') }}"></script>

    <script>
        $(document).ajaxStart(function () {
            $(".loading").show();
        });

        $(document).ajaxComplete(function () {
            $(".loading").hide();
        });
    </script>

    @yield('footer')
</body>

</html>
