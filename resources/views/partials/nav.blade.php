<nav class="navbar navbar-default navbar-fixed-top scrolled" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="fa fa-bars fa-lg"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="/img/malaria/logo.png" alt="Malaria Cure From Future" class="logo">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{url('/')}}">About</a></li>
                        <li><a href="{{url('/benefit')}}">Benefits</a></li>
                        <li><a href="{{url('/screens')}}">Features</a></li>
                        <li><a href="{{url('/support')}}">Support</a></li>
                        <li><a href="/auth/login">Login</a></li>
                        <!--<li><a href="/auth/register">Register</a></li>-->
                    @else
                        <li><a href="/home">Dashboard</a></li>
                        @if ( Auth::user()->isSuperAdmin() )
                        <li><a href="{{route('schools.index')}}">Schools</a></li>
                        <li><a href="{{url('/backup')}}">Database Backup</a></li>
                        @endif

                        @if ( !Auth::user()->isStudent() && !Auth::user()->isSuperAdmin() && !Auth::user()->isTeacher() )
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Users<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @if (!Auth::user()->isTeacher())
                                @if (!Auth::user()->isSchool())
                                <li><a href="{{route('roles.staff')}}">School Staff</a></li>
                                @endif
                                <li><a href="{{route('roles.teacher')}}">Teachers</a></li>
                                @endif
                                <li><a href="{{route('roles.student')}}">Students</a></li>
                            </ul>
                        </li>
                        @elseif (Auth::user()->isTeacher())
                            <li><a href="{{route('roles.student')}}">Students</a></li>
                        @endif

                        @if ( Auth::user()->isSchool() || Auth::user()->isTeacher() )
                        <li><a href="/classes">Classes</a></li>
                        @endif

                        @if ( Auth::user()->isSchool() )
                        <li><a href="/quizzes">Quiz Builder</a></li>
                        @elseif ( Auth::user()->isTeacher() ) 
                        <li><a href="/quizzes">Quizzes</a></li>
                        @endif

                        @if ( Auth::user()->isStudent() )
                        <li><a href="/user/quizzes">General Quizzes</a></li>
                        @endif

                        @if ( !Auth::user()->isSuperAdmin() )
                        <li><a href="/user/map">Country Map</a></li>
                        @endif

                        @if ( Auth::user()->isSchool() )
                        <li><a href="/notes">Country Information</a></li>
                        @endif
                        
                        @if ( !Auth::user()->isSuperAdmin() )
                        <li><a href="/information">Information</a></li>
                        @endif

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/user/profile">Profile</a></li>
                                <li><a href="/auth/logout">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-->
</nav>