<head>
    <meta charset="UTF-8">
    <title>Malaria - Cure From Future | Malaria Educational system</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="description" content="Malaria Educational System, Malaria Cure From Future">
    <meta name="keywords" content="Malaria, Malaria Education, Malaria Student, Malaria Prevention">
    <meta name="author" content="Kai Yu and Spagallo Team">
    <link rel="shortcut icon" href="favicon.png">

    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('/js/rs-plugin/css/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/eco.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/profile.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet">
    <link href="{{ asset('/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/css/select2.min.css"  rel='stylesheet' type='text/css'>
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"  rel='stylesheet' type='text/css'>
    

    <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet">
    
    <script type="text/javascript" src="/js/modernizr.custom.32033.js"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    @yield('css')
</head>