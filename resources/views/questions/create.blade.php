@extends('app')

@section('content')
	{!! Breadcrumbs::render('question_create',$quiz) !!}
	<div class="col-md-12 well">
	    <h1>Write a New Question</h1>
	    <hr/>

	    @include('errors.list')
	    {!! Form::open([ 'route'=>['quizzes.questions.index',$quiz->id] ]) !!}
	        @include('questions._form',['submitButtonText'=>'Create Question'])
	    {!! Form::close() !!}
	</div>
@stop

@section('footer')
	<script>
	var choiceCount = 0;
	$('.btn-add-anwser').on('click',function(e) {
		e.preventDefault();
		choiceCount++;
		var template = '<li class="list-group-item choice-container">'+
							'<div class="input-group">'+
		    					'<span class="input-group-addon">'+
		        					'<input type="radio" name="answer" value="'+choiceCount+'">'+
		      					'</span>'+
								'<input type="text" name="choices['+choiceCount+']" class="form-control" required maxlength="255">'+
								'<span class="input-group-btn"><a href="#" class="btn btn-remove">Remove</a></span>' +
							'</div>'+
        				'</li>';

		$(this).prev().append(template);
	});

	$(document).on('click','.btn-remove',function(e) {
		e.preventDefault();

		$(this).closest('.choice-container').remove();
	});
	</script>
@stop