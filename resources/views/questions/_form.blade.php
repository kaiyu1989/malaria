<div class="form-group">
	{!! Form::label('question','Question:')!!}
	{!! Form::text('question',null,['class'=>'form-control','required'])!!}
</div>

<div class="form-group">
	{!! Form::label('choices','Choices:')!!}
    <ul class="list-group">
        <li class="list-group-item">
			<div class="input-group">
	    		<span class="input-group-addon">
	        		{!! Form::radio('answer',0,['checked'=>'checked'])!!}
	      		</span>
				{!! Form::text('choices[0]',null,['class'=>'form-control','required', 'maxlength' =>255])!!}
			</div>
        </li>
    </ul>

	<a href="#" class="btn btn-sm btn-info btn-add-anwser">Add More Choice</a>
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control'])!!}
</div>