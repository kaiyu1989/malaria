@extends('app')

@section('content')
    {!! Breadcrumbs::render('question',$quiz,$question) !!}
    <div class="col-md-12 well">
        <h1>
            Question : {{$question->question}}
            <div class="btn-group">
                <a href="{{ route( 'quizzes.questions.edit',[$quiz->id,$question->id] ) }}" class="btn btn-primary">Edit Question</a>
                <a class="btn btn-danger-malaria btn-danger">Delete this Question</a>
            </div>
        </h1>

        <ul class="list-unstyled">
            <li>Updated {{$question->updated_at->diffForHumans() }} </li>
        </ul>

        <hr/>
        @if( !$question->choices->count() )
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                This question does not have any choices yet.
            </div>
        @else
            <ul class="list-group">
                @foreach( $question->choices as $choice )
                    @if( $choice->is_answer )
                        <li class="list-group-item list-group-item-success">{{$choice->choice_name}}</li>
                    @else
                        <li class="list-group-item">{{$choice->choice_name}}</li>
                    @endif
                @endforeach
            </ul>
        @endif
    </div>
@stop

@section('modal')
    <div id="deleteModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete Question</h4>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <p>Are you sure you want to delete this question - <strong id="modal-choice">{{$question->question}}</strong>?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'route' => ['quizzes.questions.destroy',$quiz->id,$question->id]] ) !!}
                        
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            <button class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
                </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script>
    $('.btn-danger-malaria').on('click',function(e) {
        e.preventDefault();
        $('#deleteModal').modal();
    });
    </script>
@stop