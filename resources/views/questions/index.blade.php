@extends('app')

@section('content')
    {!! Breadcrumbs::render('questions',$quiz) !!}
    <div class="col-md-12 well">
        <h1>
            @if($quiz->country){{$quiz->country->country_name}}@endif Quiz : {{$quiz->title}}
            <div class="btn-group">
                <a href="{{ action('QuizzesController@edit',[$quiz->id]) }}" class="btn btn-primary">Edit Quiz</a>
                <a href="{{ route( 'quizzes.questions.create',[$quiz->id] ) }}" class="btn btn-primary">Add a question</a>
            </div>
        </h1>

        <ul class="list-unstyled">
            <li>Updated {{$quiz->updated_at->diffForHumans() }} </li>
        </ul>

        <hr/>

        @if( !$quiz->questions->count() )
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                This quiz does not have any questions yet.
            </div>
        @else
            @foreach( $quiz->questions as $question )
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        <a href="{{ route( 'quizzes.questions.show',[$quiz->id,$question->id] ) }}">{{$question->question}}</a>
                        <a href="{{ route( 'quizzes.questions.edit',[$quiz->id,$question->id] ) }}" class="btn btn-primary">Edit</a>
                    </h3>
                </div>

                <div class="col-md-12">
                    <ul class="list-group">
                        @foreach( $question->choices as $choice )
                            @if( $choice->is_answer )
                                <li class="list-group-item list-group-item-success">{{$choice->choice_name}}</li>
                            @else
                                <li class="list-group-item">{{$choice->choice_name}}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
    <!--
            <article>
                <h3>
                    <a href="{{route('quizzes.questions.show',[$quiz->id,$question->id])}}">{{$question->question}}</a>
                </h3>
                <div class="body">
                    <ul class="list-group">
                        @foreach( $question->choices as $choice )
                            @if( $choice->is_answer )
                                <li class="list-group-item list-group-item-success">{{$choice->choice_name}}</li>
                            @else
                                <li class="list-group-item">{{$choice->choice_name}}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </article>
    -->
            @endforeach
        @endif
    </div>
@stop

@section('modal')
<div class="question-model modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add a question</h4>
            </div>
        
            <div class="modal-body">
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop