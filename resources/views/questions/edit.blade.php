@extends('app')

@section('content')
    {!! Breadcrumbs::render('question_edit',$quiz,$question) !!}
    <div class="col-md-12 well">
    <h1>Edit Question:{{$question->question}}</h1>

    <hr/>

    @include('errors.list')
    {!! Form::open([ 'route'=>['quizzes.questions.update',$quiz->id,$question->id],'method'=>'PATCH']) !!}
        <div class="form-group">
			{!! Form::label('question','Question:')!!}
			{!! Form::text('question',$question->question,['class'=>'form-control'])!!}
		</div>

		<div class="form-group">
			{!! Form::label('choices','Choices:')!!}
		    <ul class="list-group choice-container">
		    	@foreach($question->choices as $choice)
		        <li class="list-group-item">
					<div class="input-group">
			    		<span class="input-group-addon">
			    			{{--*/ $checked = $choice->is_answer ? 'checked="checked"':'' /*--}}
			        		<input type="radio" name="answer" value="{{$choice->id}}" {{$checked}}>
			      		</span>
						{!! Form::text('choices['.$choice->id.']',$choice->choice_name,['class'=>'form-control','required', 'maxlength' =>255])!!}
						<span class="input-group-btn"><a href="#" class="btn choice-remove" choice-id="{{$choice->id}}">Remove</a></span>
					</div>
		        </li>
		        @endforeach
		    </ul>

			<a href="#" class="btn btn-sm btn-info btn-add-anwser">Add More Choice</a>
		</div>

		<div class="form-group">
			{!! Form::submit('Update Question',['class'=>'btn btn-primary form-control'])!!}
		</div>
    {!! Form::close() !!}
    </div>
@stop

@section('modal')
	<div id="deleteModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Delete Choice</h4>
				</div>
				<div class="modal-body">
					<div class="control-group">
						<p>Are you sure you want to delete choice - <strong id="modal-choice"></strong>?</p>
					</div>
				</div>
				<div class="modal-footer">
					{!! Form::open(array('id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'route' => ['quizzes.questions.remove',$quiz->id,$question->id,''])) !!}
                        
                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
							<button class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="hidden" id="choice-id">
                    {!! Form::close() !!}
				</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script>
	var choiceCount = 0;
	$('.btn-add-anwser').on('click',function(e) {
		e.preventDefault();
		choiceCount++;
		var template = '<li class="list-group-item choice-container">'+
							'<div class="input-group">'+
		    					'<span class="input-group-addon">'+
		        					'<input type="radio" name="answer" value="new_'+choiceCount+'">'+
		      					'</span>'+
								'<input type="text" name="choices[new_'+choiceCount+']" class="form-control" required maxlength="255">'+
								'<span class="input-group-btn"><a href="#" class="btn btn-remove">Remove</a></span>' +
							'</div>'+
        				'</li>';

		$(this).prev().append(template);
	});

	$('.choice-remove').on('click',function(e) {
		e.preventDefault();
		$('#deleteModal').modal();

		choiceId = $(this).attr('choice-id');
		$('#choice-id').val(choiceId);

		$('#modal-choice').html($(this).closest('.input-group').find('input[type=text]').val());

		currentUrl = $('#delete-form').attr('action');
		$('#delete-form').attr('action', currentUrl+'/'+choiceId);

	});

	$(document).on('click','.btn-remove',function(e) {
		e.preventDefault();

		$(this).closest('.choice-container').remove();
	});
	</script>
@stop