@extends('app')

@section('content')
    <div class="col-md-12">
		{!! Breadcrumbs::render('classes_show',$class) !!}
	</div>

	<div class="col-md-4">
		@include('classes._menu',['class'=>$class])
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Class Information</h3>
			</div>
			
			<div class="panel-body">
				<!-- edit form column -->
				<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
					<table class="table">
					<tr>
						<td>Name: </td>
						<td>{{$class->name}}</td>
					</tr>
					@if(Auth::user()->isSchool())
					<tr>
						<td>Teacher: </td>
						<td>
							<a href="{{ route('users.show', [$class->teacher->id] ) }}" class="user-link">{{$class->teacher->name}}</a>
						</td>
					</tr>
					@endif
					<tr>
						<td>Total students: </td>
						<td>{{$class->students->count()}}</td>
					</tr>
					<tr>
						<td>Created at: </td>
						<td>{{$class->created_at->diffForHumans()}}</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop