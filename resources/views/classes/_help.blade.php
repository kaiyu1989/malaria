<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Tips</h3>
	</div>
	
	<div class="panel-body">
                <div class="col-md-12">
                        <span class="glyphicon glyphicon-info-sign col-md-1"></span>		
        	       <span class="text col-md-10">Each class can be assigned to one teacher at a time.</span>
                </div>
                <div class="col-md-12">
                        <span class="glyphicon glyphicon-info-sign col-md-1"></span>
                	<span class="text col-md-10">One teacher may monitor many classes.</span>
                </div>
                <div class="col-md-12">
                        <span class="glyphicon glyphicon-info-sign col-md-1"></span>
                        <span class="text col-md-10">Students can be allocated to multiple classes.</span>
                </div>
	</div>
</div>