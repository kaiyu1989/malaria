@extends('app')

@section('content')
{!! Breadcrumbs::render('classes') !!}
	<div class="col-md-3">
		@include('classes._general_menu')
	</div>

	<div class="col-md-9">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Classes</h3>
				<div class="pull-right">
	               <a href="{{route('classes.create')}}" class="btn btn-success">Create a Class</a>
	            </div>
			</div>
			
			<div class="panel-body">
				@if(!$classes->count())
				<div class="alert alert-info">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    	There is no class yet.
			    </div>
				@else
	            <div class="table-responsive" id="ulist">
					<table class="table">
						<thead>
							<tr>
								<th><span>Class Name</span></th>
								@if( Auth::user()->isSchool() )
								<th><span>Teacher</span></th>
								@endif
								<th><span>Total Number of Students</span></th>
								<th><span>Created at</span></th>
								<th>&nbsp;</th>
							</tr>
						</thead>

						<tbody>
						@foreach($classes as $class)
							<tr class="class-info" class-id="{{$class->id}}">
								<td>
									<a href="{{ route('classes.show', [$class->id] ) }}" class="ulink">{{$class->name}}</a>
								</td>
								@if( Auth::user()->isSchool() )
								<td>
									{{$class->teacher->name}}
								</td>
								@endif
								<td>
									<a href="{{ route('classes.show-students', [$class] ) }}" class="user-link">{{$class->students->count()}}</a>
								</td>
								<td>{{$class->created_at->diffForHumans()}}</td>
								<td style="width: 20%;">
									<a href="{{ route( 'classes.edit',[$class] ) }}" class="table-link">
										<span class="fa-stack fa-lg">
											<i class="fa fa-square fa-stack-2x"></i>
											<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
										</span>
									</a>
									<a href="#" class="table-link text-danger delete-user">
										<span class="fa-stack fa-lg">
											<i class="fa fa-square fa-stack-2x"></i>
											<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
										</span>
									</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				@endif
			</div>
		</div>
	</div>
@stop

@section('modal')
<div id="deleteModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Class</h4>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<p>Are you sure you want to delete class <strong id="d-name"></strong>?</p>
					<div></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::open( ['id'=>'delete-form', 'class' => 'form-inline', 'method' => 'DELETE', 'url' => ['classes']]) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
    <script>
    $('.delete-user').on('click',function(e) {
        e.preventDefault();
        $('#d-name').html($(this).closest('.class-info').find('td:nth-child(1)').html());
        classId = $(this).closest('.class-info').attr('class-id');
        $('#delete-form').attr("action",'/classes/'+classId)
        $('#deleteModal').modal();
    });
    </script>
@stop