@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('classes_results',$class) !!}
	</div>

	<div class="col-md-4">
		@include('classes._menu',['class'=>$class])
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Select a quiz to view class results - {{$class->name}}</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				
				{!! Form::open(['method'=>'POST','route'=>['classes.get-class-results',$class],'class'=>'form-horizontal']) !!}
				<div class="form-group">
					{!! Form::label('quiz_list','Quiz:',['class'=>'col-md-2 control-label']) !!}
					<div class="col-md-6">
					{!! Form::select('quiz',$quizzes,null,['id'=>'quiz_list','class'=>'form-control']) !!}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-6">
					{!! Form::button('View Results',['class'=>'btn btn-primary','id'=>'view-result'])!!}
						<i class="fa fa-3x fa-refresh fa-spin pull-right loading" style="display:none"></i>
					</div>
				</div>

				{!! Form::close() !!}
			</div>
		</div>

		<div class="panel panel-default panel-colorful" id="result-panel">
			<div class="panel-heading">
				<h3 class="panel-title">Class results for quiz - <span id="quiz-name"></span></h3>
			</div>
			
			<div class="panel-body">
				<div class="alert alert-info col-md-12" id="error-result" style="display:none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                   	<p id="error-message"></p>
                </div>
				<div class="table-responsive" id="table-result" style="display:none">
					<table class="table">
						<thead>
							<tr>
								<th><span>Student</span></th>
								<th><span>#Attempts</span></th>
								<th><span>Higest Score</span></th>
								<th><span>Lowest Score</span></th>
								<th><span>Average Score</span></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody id="result-body">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script>
	$('#quiz_list').select2({
		placeholder: 'Select a quiz',
		allowClear: true
	});

	$('#result-panel').hide();
	$('#view-result').click(function(){
		$.ajax({
			url: "{{route('classes.get-class-results',$class)}}",
			type: "post",
			data: {'quiz_id':$('select[name=quiz]').val(), '_token': $('input[name=_token]').val()},
			success: function(results){
				$('#result-panel').show();
				if(results.length>0){
					$('#error-result').hide();
					$('#table-result').show();
					$('#result-body').empty();
					$.each( results, function( index, result ){
						$('#quiz-name').html(result.quiz_title);

						elements = "<td>"+result.first_name+" "+result.last_name+"</td>";
						elements += "<td>"+result.total_attempts+"</td>";
						elements += "<td>"+result.highest_score+"/"+result.total_questions+"</td>";
						elements += "<td>"+result.lowest_score+"/"+result.total_questions+"</td>";
						elements += "<td>"+result.average_score+"/"+result.total_questions+"</td>";
						$('#result-body').append("<tr>"+elements+"</tr>");
					});
				} else {
					$('#error-result').show();
					$('#error-message').html('No class result yet.');
					$('#table-result').hide();
				}
			}
		}).fail(function() {
			$('#error-result').show();
			$('#error-message').html('Something went wrong, please try it again later.');
			$('#table-result').hide();
		});      
	});

	</script>
@endsection