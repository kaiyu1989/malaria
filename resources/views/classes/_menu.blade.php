<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon-info-sign"></span>
				<a href="{{route('classes.show',[$class])}}">Class Detail</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-edit"></span>
				<a href="{{route('classes.edit',[$class])}}">Edit Class</a>
			</li>
			<li class="list-group-item">
				<span class="fa fa-group"></span>
				<a href="{{route('classes.show-students',[$class])}}">Class Students</a>
			</li>
			<li class="list-group-item">
				<span class="glyphicon glyphicon-duplicate"></span>
				<a href="{{route('classes.display-class-results',[$class])}}">Class Results</a>
			</li>
		</ul>
	</div>
</div>