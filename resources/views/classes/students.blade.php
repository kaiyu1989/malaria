@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('classes_students',$class) !!}
	</div>

	<div class="col-md-4">
		@include('classes._menu',['class'=>$class])
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Allocate Students to Class - {{$class->name}}</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				
				{!! Form::open(['method'=>'POST','route'=>['classes.allocate-students',$class],'class'=>'form-horizontal']) !!}
				<div class="form-group">
					{!! Form::label('student_list','Students:',['class'=>'col-md-2 control-label']) !!}
					<div class="col-md-6">
					{!! Form::select('student_list[]',$students,null,['id'=>'student_list','class'=>'form-control','multiple']) !!}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-6">
					{!! Form::submit('Allocate Students',['class'=>'btn btn-primary'])!!}
					</div>
				</div>

				{!! Form::close() !!}
			</div>
		</div>

		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Current Students</h3>
			</div>
			
			<div class="panel-body">
				<div class="table-responsive" id="ulist">
					<table class="table">
						<thead>
							<tr>
								<th><span>User</span></th>
								<th><span>Email</span></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($class->students as $user)
							<tr class="user-info" user-id="{{$user->id}}">
								<td>
									<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail user-avatar">
									<a href="{{ route('roles.show-user', [$user->id] ) }}" class="ulink">{{$user->name}}</a>
								</td>
								<td>
									{{$user->email}}
								</td>
								<td style="width: 20%;">
									{!! Form::open(['method'=>'DELETE','route'=>['classes.delete-student',$class]])!!}
									{!! Form::hidden('student_id',$user->id)!!}
									{!! Form::submit('Remove',['class'=>'btn btn-danger'])!!}
									{!! Form::close() !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script>
	$('#student_list').select2({
		placeholder: 'Click here to allocate students'
	});
	</script>
@endsection