<div class="form-group">
	{!! Form::label('name','Class Name*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::text('name',null,['class'=>'form-control']) !!}
	</div>
</div>

@if( Auth::user()->isSchool() )
<div class="form-group">
	{!! Form::label('teacher_id','Teacher*',['class'=>'col-md-4 control-label'])!!}
	<div class="col-sm-6">
	{!! Form::select('teacher_id',$teachers,null,['id'=>'teacher_list','class'=>'form-control']) !!}
	</div>
	<div class="col-sm-2">
		<a target="_blank" href="{{route('roles.create-user','teacher')}}" class="btn">
			<i class="fa fa-plus"></i> Teacher
		</a>
	</div>
</div>
@else
	{!! Form::hidden('teacher_id',Auth::id(),['id'=>'teacher_list','class'=>'form-control']) !!}
@endif

<div class="form-group">
	<div class="col-sm-offset-4 col-sm-6">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary'])!!}
	</div>
</div>