@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('classes_edit',$class) !!}
	</div>

	<div class="col-md-4">
		@include('classes._menu',['class'=>$class])
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Edit Class</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($class,['method'=>'PATCH','route'=>['classes.update',$class],'class'=>'form-horizontal']) !!}
					@include('classes._form',['submitButtonText'=>'Update Class'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop