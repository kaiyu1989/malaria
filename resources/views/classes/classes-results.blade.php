@extends('app')

@section('content')
	<div class="col-md-12">
	</div>

	<div class="col-md-4">
		@include('classes._general_menu')
	</div>

	<div class="col-md-8">
		{!! Form::open(['method'=>'POST','route'=>['classes.fetch-classes-results'],'class'=>'form-horizontal']) !!}
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Filter or Export Results</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				
					<div class="form-group">
					{!! Form::label('class_id','Classes',['class'=>'col-md-4 control-label'])!!}
					<div class="col-sm-6">
					{!! Form::select('class_id',[''=>'Select a class']+$classes,$class_id?:null,['id'=>'class_list','class'=>'form-control']) !!}
					</div>
					</div>

					<div class="form-group">
					{!! Form::label('quiz_id','Quizzes',['class'=>'col-md-4 control-label'])!!}
					<div class="col-sm-6">
					{!! Form::select('quiz_id',[''=>'Select a quiz']+$quizzes,$quiz_id?:null,['id'=>'quiz_list','class'=>'form-control']) !!}
					</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							{!! Form::submit('Filter Results',['class'=>'btn btn-primary','id'=>'filter-results','name'=>'filter_results'])!!}
							<i class="fa fa-3x fa-refresh fa-spin pull-right loading" style="display:none"></i>
						</div>
					</div>
			</div>
		</div>

		<div class="panel panel-default panel-colorful" id="result-panel">
			<div class="panel-heading">
				<h3 class="panel-title">Class results</h3>
				{!! Form::submit('Export to Excel',['class'=>'btn btn-primary pull-right','id'=>'export-results','name'=>'export_results'])!!}
			</div>
			
			<div class="panel-body">
				<div class="alert alert-info col-md-12" id="error-result" style="display:none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                   	<p id="error-message"></p>
                </div>
				<div class="table-responsive" id="table-result">

					<table class="table">
						<thead>
							<tr>
								<th><span>Class</span></th>
								<th><span>Quiz</span></th>
								<th><span>Student</span></th>
								<th><span>Total #Attempts</span></th>
								<th><span>Higest Score</span></th>
								<th><span>Lowest Score</span></th>
								<th><span>Average Score</span></th>
							</tr>
						</thead>
						<tbody id="result-body">
						@foreach ( $results as $result )
							<tr class="result-row" class-id="{{$result->class_id}}">
								<td>{{$result->class_name}}</td>
								<td>{{$result->quiz_title}}</td>
								<td>{{ucfirst($result->first_name)}} {{ucfirst($result->last_name)}}</td>
								<td>{{$result->total_attempts}}</td>
								<td>{{$result->highest_score}}/{{$result->total_questions}}</td>
								<td>{{$result->lowest_score}}/{{$result->total_questions}}</td>
								<td>{{$result->average_score}}/{{$result->total_questions}}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
@stop

@section('footer')
	<script>
	$('#quiz_list').select2({
		placeholder: 'Select a quiz',
		allowClear: true
	});

	$('#class_list').select2({
		placeholder: 'Select a class',
		allowClear: true
	});

	$('.table').DataTable();

	$('.result-row').find('tr').attr('');
	</script>
@endsection