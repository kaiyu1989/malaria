<div class="panel panel-default panel-colorful">
	<div class="panel-heading">
		<h3 class="panel-title">Menu</h3>
	</div>

	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon-info-sign"></span>
				<a href="{{route('classes.index')}}">Classes</a>
			</li>
		</ul>

		<ul class="list-group">
			<li class="list-group-item">
				<span class="glyphicon glyphicon glyphicon-duplicate"></span>
				<a href="{{route('classes.show-classes-results')}}">All Classes Results</a>
			</li>
		</ul>
	</div>
</div>