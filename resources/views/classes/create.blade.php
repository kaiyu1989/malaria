@extends('app')

@section('content')
	<div class="col-md-12">
		{!! Breadcrumbs::render('users_create') !!}
	</div>

	<div class="col-md-8">
		<div class="panel panel-default panel-colorful">
			<div class="panel-heading">
				<h3 class="panel-title">Create a new Class</h3>
			</div>
			
			<div class="panel-body">
				@include('errors.list')
				{!! Form::model($class = new \App\StudentClass, ['url'=>'/classes','class'=>'form-horizontal']) !!}
					@include('classes._form',['submitButtonText'=>'Create Class'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="col-md-4">
		@include('classes._help')
	</div>
@stop