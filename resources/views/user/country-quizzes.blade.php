@extends('app')

@section('content')
    <div class="col-md-12">
    {!! Breadcrumbs::render('user_country_quiz',$country) !!}
    </div>

    <div class="col-md-4">
        @if( Auth::user()->isTeacher() || Auth::user()->isStudent() )
        <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
            <h3 class="panel-title">Quizzes</h3>
            <!--<ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                </ul>
            -->
            </div>

            <div class="panel-body" style="max-height: 300px;overflow-y: scroll;">
                @if ( $country->quizzes->count() )
                @foreach($country->quizzes as $quiz)
                <article>
                    <h4>
                        @if( Auth::user()->isStudent() )
                        <a href="{{ route( 'user.quiz-attempts',[$quiz->id] ) }}">
                            <span class="badge badge-inverse" style="font-size:18px;">{{$quiz->questions->count()}}</span>
                            Quiz : {{$quiz->title}} 
                        </a>
                        @else
                        <a href="{{ route( 'quizzes.show',[$quiz->id] ) }}">
                            <span class="badge badge-inverse" style="font-size:18px;">{{$quiz->questions->count()}}</span>
                            Quiz : {{$quiz->title}} 
                        </a>
                        @endif
                    </h4>
                </article>  
                @endforeach
                @else
                    <div class="alert alert-info col-md-12">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        No {{$country->country_name}} based quiz is published yet.
                    </div>
                @endif
            </div>
        </div>
        @endif

        <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
                <h3 class="panel-title">{{$country->country_name}} - General Information</h3>
            </div>

            <div class="panel-body">
                <ul>
                    <li>Land Area: {{$country->land_area}} &#13218;</li>
                    <li>Languages: {{$country->languages}}</li>
                </ul>
            </div>
        </div>

        @if(count($country->information))
        <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
                <h3 class="panel-title">Knowledge</h3>
                <div class="pull-right">
                    @if(Auth::user()->isSchool())
                    <a class="btn btn-success" id="edit-note">Edit</a>
                    @endif
                </div>
            </div>

            <div class="panel-body">
                <p>
                    {!! nl2br($country->information->first()->information) !!}
                </p>
            </div>
        </div>
        @endif
    </div>

    <div class="col-md-4" id="visualisation">
         <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
                <h3 class="panel-title">Reported Malaria Cases</h3>
            </div>

            <div class="panel-body">
                <div class="graph">
                    <!--<canvas id="projects-graph" ></canvas>-->
                    <div id="placeholder"></div>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
                <h3 class="panel-title">Reported Malaria Death</h3>
            </div>

            <div class="panel-body">
                <div class="graph">
                    <div id="death-placeholder"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4" id="map-section">
        <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
            <h3 class="panel-title">Please select a country on the map to view more detail.</h3>
            <ul class="panel-controls" style="margin-top: 2px;">
                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            </ul>
            </div>  
            <div class="panel-body">
                <div id="map" class="col-md-12" style="height: 400px;">

                </div>
            </div>
            <!--
            <div class="panel-footer">                                
                <button class="btn btn-lg btn-primary pull-right paceorder disabled" onclick="placeOrder()">Place Order</button>
            </div>-->
        </div>
    </div>
@stop

@section('modal')
<div id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open( ['id'=>'edit-form', 'method' => 'PATCH', 'route'=>['notes.update',$country->information->first()], 'class'=>'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Information</h4>
            </div>
            <div class="modal-body">
                @include('errors.list')
                {!! Form::label('name','Information*',['class'=>'label-control'])!!}
                {!! Form::textarea('information',count($country->information)?$country->information->first()->information:null,['class'=>'form-control']) !!}
                {!! Form::hidden('country_code',$country->id)!!}
                {!! Form::hidden('to_quiz','1')!!}
            </div>
            <div class="modal-footer">
                {!! Form::submit('Update Information',['class'=>'btn btn-success'])!!}
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('footer')
    {!! Html::script('js/Chart.min.js') !!}
    {!! Html::script('js/legend.js') !!}

    <script src="//code.highcharts.com/highcharts.js"></script>
    <script src="//code.highcharts.com/maps/highmaps.js"></script>
    <script>
    $(function(){
        $('#map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: 'transparent',
            regionStyle: {
                initial: {
                    fill: "rgb(76, 76, 76)"
                },
                hover: {
                    fill: "#fba309"
                },
                selected: {
                    fill: "#fba309"
                },
                selectedHover: {
                }
            },series: {
                regions: [{
                    values: {
                    {{$country->iso2}}:'#fba309',
                    }
                }]
            },
            onRegionClick: function(e, code){
                window.location = "/user/country-quizzes/"+code;
            }
        });
    });

    $('#edit-note').on('click',function(e) {
        e.preventDefault();
        $('#editModal').modal();
    });

    $.getJSON("{{route('visual.estimate-cases',$country)}}", function (result) {
        var labels = [],death=[],cases=[];
            
        for (var i = 0; i < result.length; i++) {
            labels.push(result[i].year);
            death.push(result[i].death);
            cases.push(result[i].cases);
        }

        if( result.length==0 ){
            /*for(var year=2000; year<=2012;year++) {
                labels.push(year);
                death.push(0);
                cases.push(0);
            }*/
            $('#placeholder').html('Data is not available for this country.');
            $('#death-placeholder').html('Data is not available for this country.');
        } else {
            var data = {
                labels : labels,
                datasets : [{
                  fillColor : "rgba(240, 127, 110, 0.3)",
                  strokeColor : "#f56954",
                  pointColor : "#A62121",
                  pointStrokeColor : "#741F1F",
                  data : death,
                  label : 'Malaria Death'
                }]
            };
            var casesDeath = {
                labels : labels,
                datasets : [{
                  fillColor : "rgba(151, 187, 205, 0.5)",
                  strokeColor : "rgba(151,187,205,1)",
                  pointColor : "rgba(151,187,205,1)",
                  pointStrokeColor : "#fff",
                  data : cases,
                  label : 'Reported Cases'
                }]
            };

            $('#placeholder').highcharts({ 
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                xAxis: {
                    categories: labels
                },
                yAxis: {
                    title: {
                        text: 'Malaria Cases'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    enabled: false,
                    floating: true,
                    verticalAlign: 'bottom',
                    align:'left',
                    layout: 'vertical',
                    padding:-20,
                },
                series: [{
                    name: 'Reported Cases',
                    data: cases
                }]
            });

            $('#death-placeholder').highcharts({ 
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                xAxis: {
                    categories: labels
                },
                yAxis: {
                    title: {
                        text: 'Malaria Death'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    enabled: false,
                    floating: true,
                    verticalAlign: 'bottom',
                    align:'left',
                    layout: 'vertical',
                    padding:-20,
                },
                series: [{
                    name: 'Reported Death',
                    data: death
                }]
            });
        }
        /*legend(document.getElementById('placeholder'), data);
        var ctx = document.getElementById('projects-graph').getContext('2d');
        new Chart(ctx).Line(data, { bezierCurve : true });

        legend(document.getElementById('death-placeholder'), casesDeath);
        var death_ctx = document.getElementById('death-graph').getContext('2d');
        new Chart(death_ctx).Line(casesDeath, { bezierCurve : true });*/
    });
    </script>


    @if ($errors->any())
    <script>
        $('#editModal').modal();
    </script>
    @endif
    {!! Html::script('js/malaria/panel-fullscreen.js') !!}
@stop