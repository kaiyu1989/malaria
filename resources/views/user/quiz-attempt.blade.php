@extends('app')

@section('content')
    {!! Breadcrumbs::render('user_country_quiz_attempt',$quiz) !!}
    <div class="col-md-12 well">
        <h1>
            @if($quiz->country){{$quiz->country->country_name}}@endif Quiz : {{$quiz->title}}
        </h1>

        <ul class="list-unstyled">
            <li>{{$quiz->questions->count()}} questions in total</li>
        </ul>

        <hr/>

        @if( !$quiz->questions->count() )
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                This quiz does not have any questions yet.
            </div>
        @else
            {!! Form::open([ 'route'=>['user.quiz-attempt.store',$quiz->id] ]) !!}
            @foreach( $quiz->questions()->randomOrder()->get() as $question )
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        {{$question->question}}
                    </h3>
                </div>
                <div class="col-md-12">
                    <ul class="list-group">
                        @foreach( $question->choices()->randomOrder()->get() as $choice )
                            <li class="list-group-item">
                            {!! Form::radio('choices['.$question->id.']',$choice->id)!!}

                            {{$choice->choice_name}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endforeach
            {!! Form::submit('Submit Quiz',['class'=>'btn btn-primary form-control'])!!}
            {!! Form::close() !!}

        @endif
    </div>
@stop

@section('footer')
    <script>
    $(function() {
        $('.list-group-item').on('click',function(){
            $(this).parent().find('input:radio').removeProp('checked');
            
            $(this).children().attr('checked',true);
            $(this).children().prop('checked',true);
        });
    });
    </script>
@stop