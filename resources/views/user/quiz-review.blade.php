@extends('app')

@section('content')
    {!! Breadcrumbs::render('user_country_quiz_review',$quiz) !!}

    <div class="col-md-12 well">
        <h1>
            Review @if($quiz->country){{$quiz->country->country_name}}@endif Quiz : {{$quiz->title}}
        </h1>

        <ul class="list-unstyled">
            <li>{{$quiz->questions->count()}} questions in total</li>
            <li>
            	You score is {{$quizAttempt->score}}/{{$quiz->questions->count()}}
            	@if ( $quizAttempt->score / $quiz->questions->count() >= 0.6) 
            		<i class="fa fa-smile-o fa-2x"></i>
            	@elseif ( $quizAttempt->score / $quiz->questions->count() < 0.3) 
            		<i class="fa fa-frown-o fa-2x"></i>
            	@else
            		<i class="fa fa-meh-o fa-2x"></i>
            	@endif
            </li>
        </ul>

        <hr/>

        @if( !$quiz->questions->count() )
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Nothing can be reviewed.
            </div>
        @else
        	@foreach( $quizAttempt->quizResponses as $response)
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        {{$response->question->question}}
                    </h3>
                </div>
                <div class="col-md-12">
                    <ul class="list-group">
                    	@foreach( $response->question->choices as $choice )
    	                	@if( $choice->is_answer )                    
    	                		<li class="list-group-item list-group-item-success">
    	                			<span class="label label-success">Right Answer</span>
    	                			@if( $response->choice_id == $choice->id )
    	                			<span class="label label-success">Your Answer</span>
    	                			@endif
    	                	@elseif( ($response->choice_id == $choice->id) && !$choice->is_answer )
    	                		<li class="list-group-item list-group-item-danger">
    	                			<span class="label label-danger">Your Answer</span>
    	                	@else
    	                		<li class="list-group-item">
    	                	@endif
    	                     {{$choice->choice_name}}
    	                    </li>
                    	@endforeach
                    </ul>
                </div>
            </div>
            @endforeach
           	<a href="{{ route( 'user.quiz-attempts',[$quiz->id] ) }}" class="btn btn-primary">Finish Review</a>
        @endif
    </div>
@stop