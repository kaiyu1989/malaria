@extends('app')

@section('css')
	<link href="{{ asset('/css/horizon-nav.css') }}" rel="stylesheet">
@stop

@section('content')
	{!! Breadcrumbs::render('user_profile_password') !!}
    <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <a href="{{ route( 'user.profile' ) }}" class="btn btn-nav">
                <span class=" glyphicon glyphicon-user"></span>
                <p>Profile Information</p>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{ route( 'user.profile-edit' ) }}" class="btn btn-nav">
                <span class="glyphicon glyphicon-edit"></span>
                <p>Edit Profile</p>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{ route( 'user.profile-password' ) }}" class="btn btn-nav active">
                <span class="glyphicon glyphicon-lock"></span>
                <p>Update Password</p>
            </a>
        </div>
    </div>

	<div class="col-md-12 well">
		{!! Form::open(['url' => action('UserController@postProfilePassword'), 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal']) !!}
		<!-- left column -->
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="text-center">
				@if( $user->avatar )
					<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail">
				@else
					<img src="/img/malaria/default-user-icon-profile.png" alt="" class="avatar img-circle img-thumbnail">
				@endif
			</div>
		</div>
		<!-- edit form column -->
		<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
			@include('errors.list')
			<h3>Update Password</h3>
			<div class="form-group">
				{!! Form::label('password','Current Password',['class'=>'col-md-4 control-label'])!!}
				<div class="col-md-6">
					{!! Form::password('password',['class'=>'form-control','autocomplete'=>'off']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('password','New Password',['class'=>'col-md-4 control-label'])!!}
				<div class="col-md-6">
					{!! Form::password('new_password',['class'=>'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('password','New Password Confirmation',['class'=>'col-md-4 control-label'])!!}
				<div class="col-md-6">
					{!! Form::password('new_password_confirmation',['class'=>'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					{!! Form::hidden('is_general',0) !!}
					{!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
				</div>
			</div>
		</div>

		{!! Form::close() !!}
	</div>
@endsection
