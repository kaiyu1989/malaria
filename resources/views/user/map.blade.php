@extends('app')

@section('content')
    <div class="col-md-12">
        {!! Breadcrumbs::render('user_map') !!}
    </div>

    @if ( !$quizzes->count() )
        <div class="alert alert-info col-md-12">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            No quiz is published yet.
        </div>
    @else
        <div class="col-md-4">
            <div class="panel panel-default panel-colorful">
                <div class="panel-heading">
                <h3 class="panel-title">Do you know?</h3>
                <!--<ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    </ul>
                -->
                </div>

                <div class="panel-body">
                    <p>Hello! I am Malaria.</p>
                    <p>Unfortunately, I cannot be a good friend of yours, because I am naturally harmful.</p>
                    <p>Many people around the world become sick or die because of me.</p>
                    <p>
                    If you contract me, you will get <b>fever</b>, <b>fatigue</b>, <b>vomiting</b>, <b>headaches</b>.
                    I am prevalent in many tropical countries especially in the continents of Africa and Asia.
                    </p>
                    <p>I have no friends but mosquitos - my transmitter.</p>
                    <p>You probably want to avoid me. To do that you need to learn more about me, where I live, and how my friends, the mosquitos help me spread. If you study the map, and then you will know how to avoid my harmful effects and learn my severity.</p>
                </div>
                <!--
                <div class="panel-footer">                                
                    <button class="btn btn-lg btn-primary pull-right paceorder disabled" onclick="placeOrder()">Place Order</button>
                </div>-->
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel panel-default panel-colorful">
                <div class="panel-heading">
                <h3 class="panel-title">Please select a country on the map to view more detail.</h3>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    </ul>
                </div>  
                <div class="panel-body">
                    <div id="map" class="col-md-12" style="height: 540px;">

                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('footer')
<script>
    $(function(){
        $('#map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: 'transparent',
            regionStyle: {
                initial: {
                    fill: "rgb(76, 76, 76)"
                },
                hover: {
                    fill: "#fba309"
                },
                selected: {
                    fill: "#fba309"
                },
                selectedHover: {
                }
            },
            onRegionClick: function(e, code){
                window.location = "/user/country-quizzes/"+code;
            }
        });
    });
</script>

{!! Html::script('js/malaria/panel-fullscreen.js') !!}
@stop