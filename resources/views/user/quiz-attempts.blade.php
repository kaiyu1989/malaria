@extends('app')

@section('css')
<link href="{{ asset('/css/event-list.css') }}" rel="stylesheet">
@stop

@section('content')
    {!! Breadcrumbs::render('user_country_quiz_attempts',$quiz) !!}
    <div class="col-md-12 well">
        <h1>
            @if($quiz->country){{$quiz->country->country_name}}@endif Quiz : {{$quiz->title}}
            <a href="{{ route( 'user.quiz-attempt.show',[$quiz->id] ) }}" class="btn btn-info">Attempt Quiz</a>
        </h1>

        <ul class="list-unstyled">
            <li>{{$quiz->questions->count()}} questions in total</li>
            @if( $quizAttempts->count() )
            <li>
            	Your highest score is {{$quizAttempts->max('score')}} / {{$quiz->questions->count()}}
            	@if ( $quizAttempts->max('score') / $quiz->questions->count() >= 0.6) 
            		<i class="fa fa-smile-o fa-2x"></i>
            	@elseif ( $quizAttempts->max('score') / $quiz->questions->count() < 0.3) 
            		<i class="fa fa-frown-o fa-2x"></i>
            	@else
            		<i class="fa fa-meh-o fa-2x"></i>
            	@endif
            </li>
            @endif
        </ul>

        <hr/>
        @if( !$quizAttempts->count() )
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            You may want to start your first attempt of <strong>Quiz : {{$quiz->title}}</strong>.
        </div>
        @else
		<div class="row">
			<div class="[ col-xs-12 col-sm-8 ]">
				<ul class="event-list">
			        @foreach( $quizAttempts as $attempt )
			        <li>
						<time datetime="{{$attempt->created_at}}">
							<!--<span class="day">{{$attempt->created_at->day}}</span>
							<span class="month">{{$attempt->created_at->format('M')}}</span>
							<span class="year">{{$attempt->created_at->year}}</span>
							<span class="time">{{$attempt->created_at->toTimeString()}}</span>-->
                            <span class="month">Score</span>
                            <span class="day">{{$attempt->score}}</span>
						</time>
						<!--<img alt="Independence Day" src="https://farm4.staticflickr.com/3100/2693171833_3545fb852c_q.jpg" />-->
						<div class="info">
							<h2 class="title"><a href="{{ route( 'user.quiz-attempt.review',[$attempt->id] ) }}" >Review</a></h2>
							<p class="desc">
                                Attempted at {{$attempt->created_at->format('d M Y g:i A')}}
                            </p>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
        @endif
    </div>
@stop