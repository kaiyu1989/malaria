@extends('app')

@section('content')
    <div class="col-md-12">
        {!! Breadcrumbs::render('user_general_quiz') !!}
    </div>

    @if ( !$quizzes->count() )
    <div class="col-md-12">
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            No general quiz is published yet.
        </div>
    </div>
    @else
    <div class="col-md-12">
        <div class="panel panel-default panel-colorful">
            <div class="panel-heading">
                <h3 class="panel-title">General Quizzes</h3>
            </div>

            <div class="panel-body">
                @foreach($quizzes as $quiz)
                <article>
                    <h2>
                        <a href="{{ route( 'user.quiz-attempts',[$quiz->id] ) }}">

                            <span class="badge badge-inverse" style="font-size:20px;">{{$quiz->questions->count()}}</span>
                            @if($quiz->country)
                            {{$quiz->country->country_name}}
                            @endif
                            Quiz : {{$quiz->title}} 
                        </a>
                    </h2>
                </article>  

                @endforeach
            </div>
        </div>
    </div>
    @endif

@stop