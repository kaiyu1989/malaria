@extends('app')

@section('css')
    <link href="{{ asset('/css/horizon-nav.css') }}" rel="stylesheet">
@stop

@section('content')
    {!! Breadcrumbs::render('user_profile') !!}
    <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <a href="{{ route( 'user.profile' ) }}" class="btn btn-nav active">
                <span class=" glyphicon glyphicon-user"></span>
                <p>Profile Information</p>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{ route( 'user.profile-edit' ) }}" class="btn btn-nav">
                <span class="glyphicon glyphicon-edit"></span>
                <p>Edit Profile</p>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{ route( 'user.profile-password' ) }}" class="btn btn-nav">
                <span class="glyphicon glyphicon-lock"></span>
                <p>Update Password</p>
            </a>
        </div>
    </div>

    <div class="col-md-12 well">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="text-center">
                <img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail">
            </div>

            @if($user->isStudent())
            <div class="col-xs-12 col-sm-4 emphasis">
                <h2><strong> {{$user->quizAttempts->groupBy('quiz_id')->count()}} </strong></h2>
                <p><small>Quizzes</small></p>
                <!--<button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Quizzes </button>-->
            </div>

            <div class="col-xs-12 col-sm-4 emphasis">
                <h2><strong> {{$user->quizAttempts->count()}} </strong></h2>                 
                <p><small>Quiz Attempts</small></p>
            </div>
            @endif
        </div>
        <!-- edit form column -->
        <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
            <h2>{{$user->name}}</h2>
            <p><strong>Email: </strong> {{$user->email}} </p>
            <p>Joined since {{$user->created_at->diffForHumans()}}</p>
            <a class="btn btn-info" href="{{url('/user/profile-edit')}}"><span class="fa fa-user"></span> Edit Profile </a>
        </div>
    </div>
@stop