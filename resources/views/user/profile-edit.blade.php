@extends('app')

@section('css')
<link href="{{ asset('/css/horizon-nav.css') }}" rel="stylesheet">
@stop

@section('content')
	{!! Breadcrumbs::render('user_profile_edit') !!}
    <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <a href="{{ route( 'user.profile' ) }}" class="btn btn-nav">
                <span class=" glyphicon glyphicon-user"></span>
                <p>Profile Information</p>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{ route( 'user.profile-edit' ) }}" class="btn btn-nav active">
                <span class="glyphicon glyphicon-edit"></span>
                <p>Edit Profile</p>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{ route( 'user.profile-password' ) }}" class="btn btn-nav">
                <span class="glyphicon glyphicon-lock"></span>
                <p>Update Password</p>
            </a>
        </div>
    </div>

	<div class="col-md-12 well">
		{!! Form::open(['url' => action('UserController@postProfileEdit'), 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal']) !!}
		<!-- left column -->
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="text-center">
				@if( $user->avatar )
					<img src="{{$user->avatar->url()}}" class="profile-img-card avatar img-circle img-thumbnail">
				@else
					<img src="/img/malaria/default-user-icon-profile.png" alt="" class="avatar img-circle img-thumbnail">
				@endif

				<h6>Upload a different photo...</h6>
				{!! Form::file('avatar',['class'=>'text-center center-block well well-sm']) !!}
			</div>
		</div>
		<!-- edit form column -->
		<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
			@include('errors.list')
			<h3>Personal info</h3>
			
			<div class="form-group">
				{!! Form::label('first_name','First Name',['class'=>'col-md-4 control-label'])!!}
				<div class="col-md-6">
					{!! Form::text('first_name',$user->first_name,['class'=>'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('last_name','Last Name',['class'=>'col-md-4 control-label'])!!}
				<div class="col-md-6">
					{!! Form::text('last_name',$user->last_name,['class'=>'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('email','E-Mail Address',['class'=>'col-md-4 control-label'])!!}
				<div class="col-md-6">
					{!! Form::text('email',$user->email,['class'=>'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					{!! Form::hidden('is_general',1) !!}
					{!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
				</div>
			</div>
		</div>
	</div>

	{!! Form::close() !!}
</div>
@endsection
