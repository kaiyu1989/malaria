@extends('app')

@section('content')
<div class="row">
	<div class="col-md-12">
	{!! Form::open(['url' => action('UsersController@update',11), 'method' => 'PATCH', 'files' => true]) !!}
	    {!! Form::text('name',null,['class'=>'form-control']) !!}
	    {!! Form::file('avatar') !!}
	    {!! Form::submit('Save',['class'=>'form-control']) !!}
	{!! Form::close() !!}
	</div>
</div>
@endsection
