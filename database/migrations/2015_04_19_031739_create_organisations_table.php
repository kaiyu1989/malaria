<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organisations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 40);
			$table->string('description', 255);
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');
		});

		Schema::table('users', function($table) {
			$table->foreign('organisation_id')
				->references('id')
				->on('organisations');
		});

		Schema::table('quizzes', function($table) {
			$table->integer('organisation_id')->unsigned()->index()->nullable();

			$table->foreign('organisation_id')
					->references('id')
					->on('organisations')
					->onDelete('cascade');
		});

		Schema::create('classes', function($table)
		{
			$table->increments('id');
			$table->string('name', 40);
			$table->integer('organisation_id')->unsigned()->index();
	        $table->integer('teacher_id')->unsigned()->index();
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');

			$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
	        $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::create('class_student', function(Blueprint $table) {
	        $table->integer('class_id')->unsigned()->index();
	        $table->integer('student_id')->unsigned()->index();
	        $table->foreign('class_id')->references('id')->on('classes')->onDelete('cascade');
	        $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');
    	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('class_student');
		Schema::drop('classes');
		Schema::table('quizzes', function($table) {
			$table->dropForeign('quizzes_organisation_id_foreign');
		});
		Schema::table('users', function($table) {
			$table->dropForeign('users_organisation_id_foreign');
			$table->dropColumn('organisation_id');
		});
		Schema::drop('organisations');
	}

}
