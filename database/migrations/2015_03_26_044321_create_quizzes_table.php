<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quizzes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('user_id')->unsigned();
			$table->string('country_code',5)->nullable();
			//$table->timestamps();
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');

			$table->foreign('user_id')
					->references('id')
					->on('users')
					->onDelete('cascade');
		});

		Schema::create('questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quiz_id')->unsigned();
			$table->string('question');
			//$table->integer('answer_id')->unsigned()->nullable();
			//$table->timestamps();
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');

			$table->foreign('quiz_id')
					->references('id')
					->on('quizzes')
					->onDelete('cascade');

			/*$table->foreign('answer_id')
					->references('id')
					->on('choices');*/
		});

		Schema::create('choices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('question_id')->unsigned();
			$table->string('choice_name');
			$table->tinyInteger('is_answer')->default(0);
			//$table->timestamps();
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');

			$table->foreign('question_id')
					->references('id')
					->on('questions')
					->onDelete('cascade');
		});

		Schema::create('quiz_attempts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('quiz_id')->unsigned();
			$table->tinyInteger('score');
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');
			

			$table->foreign('user_id')
					->references('id')
					->on('users')
					->onDelete('cascade');

			$table->foreign('quiz_id')
					->references('id')
					->on('quizzes')
					->onDelete('cascade');
		});

		Schema::create('quiz_responses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quiz_attempt_id')->unsigned();
			$table->integer('quiz_id')->unsigned();
			$table->integer('question_id')->unsigned();
			$table->integer('choice_id')->unsigned();
			$table->integer('user_id')->unsigned();
			//$table->timestamps();
			$table->bigInteger('created_at');
			$table->bigInteger('updated_at');

			$table->foreign('quiz_attempt_id')
					->references('id')
					->on('quiz_attempts')
					->onDelete('cascade');

			$table->foreign('quiz_id')
					->references('id')
					->on('quizzes')
					->onDelete('cascade');
					
			$table->foreign('question_id')
					->references('id')
					->on('questions')
					->onDelete('cascade');

			$table->foreign('choice_id')
					->references('id')
					->on('choices')
					->onDelete('cascade');

			$table->foreign('user_id')
					->references('id')
					->on('users')
					->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quiz_responses');
		Schema::drop('quiz_attempts');
		Schema::drop('choices');
		Schema::drop('questions');
		Schema::drop('quizzes');
	}

}
