<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMalariaVisualisationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regions', function(Blueprint $table)
		{
			$table->string('id',20);
			$table->string('region_name');
			$table->primary('id');
		});

		Schema::create('countries', function(Blueprint $table)
		{
			$table->string('id',5);
			$table->string('country_name');
			$table->string('region_code',20);
			$table->string('iso2',2)->nullable();
			$table->integer('land_area')->unsigned()->default(0);
			$table->string('languages')->nullable();
			
			$table->primary('id');

			$table->foreign('region_code')
					->references('id')
					->on('regions')
					->onDelete('cascade');
		});

		Schema::create('region_report', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('region_code',20);
			$table->integer('death')->unsigned();
			$table->integer('cases')->unsigned();
			$table->integer('estimate_death')->unsigned();
			$table->integer('estimate_cases')->unsigned();
			$table->smallInteger('year')->unsigned();

			$table->foreign('region_code')
					->references('id')
					->on('regions')
					->onDelete('cascade');
		});

		Schema::create('country_report', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('country_code',5);
			$table->integer('death')->unsigned();
			$table->integer('cases')->unsigned();
			$table->integer('estimate_death')->unsigned();
			$table->integer('estimate_cases')->unsigned();
			$table->smallInteger('year')->unsigned();

			$table->foreign('country_code')
					->references('id')
					->on('countries')
					->onDelete('cascade');
		});

		Schema::create('population', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('country_code',5);
			$table->integer('population')->unsigned();
			$table->smallInteger('year')->unsigned();

			$table->foreign('country_code')
					->references('id')
					->on('countries')
					->onDelete('cascade');
		});

		Schema::create('gdp', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('country_code',5);
			$table->decimal('amount', 10, 2);
			$table->smallInteger('year')->unsigned();

			$table->foreign('country_code')
					->references('id')
					->on('countries')
					->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gdp');
		Schema::drop('population');
		Schema::drop('country_report');
		Schema::drop('region_report');
		Schema::drop('countries');
		Schema::drop('regions');
	}

}
