<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CountryReport;
use App\Country;
class WHOSeeder extends Seeder{

	/**
     * read WHO json file and save data to country report table
     *
     */
    public function run()
    {
        //DB::unprepared(file_get_contents(app_path()."/../database/seeds/factors.sql"));
        $this->runMalariaEstimateCases();
        $this->runMalariaConfirmedCases();
        $this->runMalariaEstimateDeath();
        $this->runMalariaConfirmedDeath();
    }


    /**
     * run malaria estimate cases from WHO
     *
     */
    public function runMalariaEstimateCases()
    {
    	$link = 'http://apps.who.int/gho/athena/data/GHO/MALARIA002.json?filter=COUNTRY:*';

    	$this->runCountryReportJson($link, 'estimate_cases');
    }

    /**
     * run malaria confirmed death from WHO
     *
     */
    public function runMalariaConfirmedCases()
    {
    	$link = 'http://apps.who.int/gho/athena/data/GHO/WHS3_48.json?filter=COUNTRY:*';

    	$this->runCountryReportJson($link, 'cases');
    }

    /**
     * run malaria estimate death from WHO
     *
     */
    public function runMalariaEstimateDeath()
    {
		$link = 'http://apps.who.int/gho/athena/data/GHO/MALARIA003.json?filter=COUNTRY:*';

		$this->runCountryReportJson($link, 'estimate_death');
	}

	/**
     * run malaria confirmed death from WHO
     *
     */
    public function runMalariaConfirmedDeath()
    {
    	$link = 'http://apps.who.int/gho/athena/data/GHO/MALARIA001.json?filter=COUNTRY:*';

		$this->runCountryReportJson($link, 'death');
    }

    /**
     * run script to load data into the country_report table
     *
     * @param $link link of WHO of different json file with same internal structure
     * @param $field table field name to fill the data
     */
    private function runCountryReportJson($link, $field){
    	$malariaJsonFile = file_get_contents($link);
		$json = json_decode($malariaJsonFile);

		foreach( $json->fact as $fact ){
			$field_value = $fact->value->numeric?:0;
			foreach( $fact->Dim as $attr ) {
				if( $attr->category == 'COUNTRY' ) {
					$country_code = $attr->code;
				} elseif ( $attr->category == 'YEAR' ) {
					$year = $attr->code;
				}
			}

            if( Country::find($country_code) ) {
    			$country = CountryReport::whereCountryCodeAndYear($country_code,$year)->first();
    			if( $country ){
    				$country->update([$field=>$field_value]);
    			} else {
    				CountryReport::create([
    					'country_code' => $country_code,
    					$field => $field_value,
    					'year' => $year
    				]);
    			}
            } else {
                echo $country_code." does not exist in the country list. Ignored.\n";
            }
		}
    }
}