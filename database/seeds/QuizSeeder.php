<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Quiz;
use App\Question;
use App\Choice;
use App\User;
class QuizSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
        DB::table('quizzes')->delete();
        DB::table('questions')->delete();
        DB::table('choices')->delete();
		DB::unprepared(file_get_contents(app_path()."/../database/seeds/quizzes.sql"));

		/*
		$faker = Faker\Factory::create();
		
		$user = User::first();
		$theQuiz = $user->quizzes()->create(['title' => 'Malaria General','organisation_id'=>1]);


		$q1= $theQuiz->questions()->create(['question'=>'Malaria is spread by ___________?']);
		$q2= $theQuiz->questions()->create(['question'=>'Symptoms of malaria are fever, chills and sweating, etc.']);
		$q3= $theQuiz->questions()->create(['question'=>'A patient with malaria may suffer from jaundice.']);
		$q4= $theQuiz->questions()->create(['question'=>'A blood test is always positive in a case of malaria.']);
		$q5= $theQuiz->questions()->create(['question'=>'What organ in your body does malaria affect the most?']);
		$q6= $theQuiz->questions()->create(['question'=>'Malaria is mainly found in _________?']);
		$q7= $theQuiz->questions()->create(['question'=>'Dizziness is a symptom of malaria.']);
		$q8= $theQuiz->questions()->create(['question'=>'Where is malaria most common within Africa?']);
		$q9= $theQuiz->questions()->create(['question'=>'All mosquitoes carry malaria.']);
		$q10= $theQuiz->questions()->create(['question'=>'How many countries and territories worldwide are considered at risk for malarial infections?']);

		
		$q1->choices()->create(['choice_name' => 'Inhaling bad air','is_answer' => '0']);
		$q1->choices()->create(['choice_name' => 'Drinking unclean water','is_answer' => '0']);
		$q1->choices()->create(['choice_name' => 'Bite of a mosquito','is_answer' => '1']);
		$q1->choices()->create(['choice_name' => 'Eating roadside food','is_answer' => '0']);
		$q2->choices()->create(['choice_name' => 'True','is_answer' => '1']);
		$q2->choices()->create(['choice_name' => 'False','is_answer' => '0']);
		$q3->choices()->create(['choice_name' => 'True','is_answer' => '1']);
		$q3->choices()->create(['choice_name' => 'False','is_answer' => '0']);
		$q4->choices()->create(['choice_name' => 'True','is_answer' => '0']);
		$q4->choices()->create(['choice_name' => 'False','is_answer' => '1']);
		$q5->choices()->create(['choice_name' => 'Heart','is_answer' => '0']);
		$q5->choices()->create(['choice_name' => 'Brain','is_answer' => '0']);
		$q5->choices()->create(['choice_name' => 'Small intestine','is_answer' => '0']);
		$q5->choices()->create(['choice_name' => 'Liver','is_answer' => '1']);
		$q6->choices()->create(['choice_name' => 'Africa','is_answer' => '1']);
		$q6->choices()->create(['choice_name' => 'Canada','is_answer' => '0']);
		$q6->choices()->create(['choice_name' => 'Australia','is_answer' => '0']);
		$q6->choices()->create(['choice_name' => 'Alaska','is_answer' => '0']);
		$q7->choices()->create(['choice_name' => 'True','is_answer' => '0']);
		$q7->choices()->create(['choice_name' => 'False','is_answer' => '1']);
		$q8->choices()->create(['choice_name' => 'Southern Africa','is_answer' => '0']);
		$q8->choices()->create(['choice_name' => 'Central Africa','is_answer' => '1']);
		$q8->choices()->create(['choice_name' => 'Western Africa','is_answer' => '0']);
		$q8->choices()->create(['choice_name' => 'Northern Africa','is_answer' => '0']);
		$q9->choices()->create(['choice_name' => 'True','is_answer' => '0']);
		$q9->choices()->create(['choice_name' => 'False','is_answer' => '1']);
		$q10->choices()->create(['choice_name' => 'Over 100','is_answer' => '1']);
		$q10->choices()->create(['choice_name' => 'Less than 50','is_answer' => '0']);
		$q10->choices()->create(['choice_name' => 'Between 50 and 100','is_answer' => '0']);
		$q10->choices()->create(['choice_name' => 'Almost all countries','is_answer' => '0']);
		*/
	}

}
