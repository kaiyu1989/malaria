<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RoleTableSeeder extends Seeder{

    public function run()
    {

        DB::table('roles')->delete();

        Role::create([
            'id'            => 1,
            'name'          => 'Super Administrator',
            'description'   => 'Access to manage different schools.'
        ]);

        Role::create([
            'id'            => 2,
            'name'          => 'Administrator',
            'description'   => 'Full access to the system.'
        ]);

        Role::create([
            'id'            => 3,
            'name'          => 'School Staff',
            'description'   => 'Full access as school staff.'
        ]);

        Role::create([
            'id'            => 4,
            'name'          => 'Teacher',
            'description'   => 'Full access as teacher.'
        ]);

        Role::create([
            'id'            => 5,
            'name'          => 'Student',
            'description'   => 'Full access as student.'
        ]);
    }

}