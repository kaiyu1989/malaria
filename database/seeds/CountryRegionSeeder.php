<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Region;
class CountryRegionSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		/*if (App::environment() === 'production') {
			exit('You are under production.');
		}*/

		// Uncomment the below to wipe the table clean before populating
        DB::table('countries')->delete();
        DB::table('regions')->delete();
        
 		// Uncomment to add records manually from sql file
 		//DB::unprepared(file_get_contents(app_path()."/../database/seeds/regioncountry.sql"));

        //Uncomment to add records from WHO website JSON files
        $this->runRegion();
        $this->runCountry();
		$this->updateCountryISO2();
	}


	/**
	 * fill region data from WHO
	 *
	 * @return void
	 */
	private function runRegion()
	{
		$regionJson = file_get_contents('http://apps.who.int/gho/athena/data/REGION.json');
		$json = json_decode($regionJson);

		$regions = $json->dimension[0]->code;
		if($regions) 
		{
			foreach( $regions as $region )
			{
				$region_code = $region->label;
				$region_name = $region->display;
				Region::create(['id'=>$region_code,'region_name'=>$region_name]);
			}
		}
	}

	/**
	 * fill country data from WHO
	 *
	 * @return void
	 */
	private function runCountry()
	{
		$countryJson = file_get_contents('http://apps.who.int/gho/athena/data/COUNTRY.json');
		$json = json_decode($countryJson);
		
		$countries = $json->dimension[0]->code;
		if($countries) 
		{
			foreach( $countries as $country ) 
			{
				$country_code = $country->label;
				$country_name = $country->display;

				$region_code = 'NOTSPEC';
				$land_area = '0';
				$languages = $iso2 = '';
				foreach( $country->attr as $attr)
				{
					if($attr->category == 'WHO_REGION_CODE') {
						$region_code = $attr->value?:$region_code;
					} elseif ($attr->category == 'ISO2') {
						$iso2 = $attr->value?:$iso2;
					} elseif ($attr->category == 'LAND_AREA_KMSQ_2012') {
						$land_area = $attr->value?:$land_area;
					} elseif ($attr->category == 'LANGUAGES_EN_2012') {
						$languages = $attr->value?:$languages;
					}
				}

				if( !in_array($country_code,['X10','X11','X12']) ) {
					Country::create([
							'id' => $country_code,
							'country_name' => $country_name,
							'region_code' => $region_code,
							'iso2' => $iso2,
							'land_area' => str_replace(',', '', $land_area),
							'languages' => $languages
						]);
				}
			}
		}
	}

	/**
	 * fill the missing iso2 country code in WHO from github rep to the database
	 *
	 * @return void
	 */
	private function updateCountryISO2()
	{
		$countryJson = file_get_contents('https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.json');
		$json = json_decode($countryJson);
		if($json){
			foreach($json as $item){
				if($item->{"alpha-3"}=="TWN"){
					$country = Country::find('XX1');
				} else {
					$country = Country::find($item->{"alpha-3"});
				}

				if($country){
					$country->update(['iso2'=>$item->{"alpha-2"}]);
				}
			}
		}
	}
}