<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CountryRegionSeeder');
		$this->call('RoleTableSeeder');
		$this->call('UserSeeder');
		$this->call('QuizSeeder');
		$this->call('InformationSeeder');
		$this->call('WHOSeeder');
	}

}
