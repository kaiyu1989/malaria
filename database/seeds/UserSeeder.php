<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Organisation;
use App\StudentClass;
class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
        DB::table('users')->delete();
		$organisation = Organisation::create(['name' => 'The School','description'=>"An awesome school"]);

		$superAdmin = User::create([
			'first_name' => 'MCFF',
			'last_name' => 'Super Admin',
			'email' => 'super.admin@mcff.tk',
			'password' => Hash::make('password'),
			'role_id' => 1,
			'organisation_id' => $organisation->id
		]);

		$admin = User::create([
			'first_name' => 'MCFF',
			'last_name' => 'Admin',
			'email' => 'admin@mcff.tk',
			'password' => Hash::make('password'),
			'role_id' => 2,
			'organisation_id' => $organisation->id
		]);

		$school = User::create([
			'first_name' => 'MCFF',
			'last_name' => 'School',
			'email' => 'school@mcff.tk',
			'password' => Hash::make('password'),
			'role_id' => 3,
			'organisation_id' => $organisation->id
		]);
		

		$teacher = User::create([
			'first_name' => 'MCFF',
			'last_name' => 'Teacher',
			'email' => 'teacher@mcff.tk',
			'password' => Hash::make('password'),
			'role_id' => 4,
			'organisation_id' => $organisation->id
		]);

		$student = User::create([
			'first_name' => 'MCFF',
			'last_name' => 'Student',
			'email' => 'student@mcff.tk',
			'password' => Hash::make('password'),
			'role_id' => 5,
			'organisation_id' => $organisation->id
		]);

		$classA = StudentClass::create([
			'name' => 'Class A',
			'organisation_id' => $organisation->id,
			'teacher_id' => $teacher->id
		]);

		$classB = StudentClass::create([
			'name' => 'Class B',
			'organisation_id' => $organisation->id,
			'teacher_id' => $teacher->id
		]);

		$classA->students()->attach($student);

		


	}

}
