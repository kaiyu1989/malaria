<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Information;

class InformationSeeder extends Seeder{

    public function run()
    {

        DB::table('information')->delete();
        DB::unprepared(file_get_contents(app_path()."/../database/seeds/country-information.sql"));
        
        /*Information::create([
            'information'		=> 'Capital City - Kuala Lumpur
Official Language - Malay
Official Religion - Islam
Independence Date - 31 August 1957
GDP
Currency - Ringgit Malaysia (RM) (MYR)
States - 14 states 
Johor, Kedah, Kelantan, Malacca, Negeri Sembilan, Pahang, Penang, Perak, Perlis, Sabah, Sarawak, Selangor and Terengganu.
Culture - Three main ethnic groups - Malay(50%) , Chinese(25%) , Indian(10%) , Others (15%) 
** Others include Indigenous Ethnic Groups

Festivals - 
Hari Raya Puasa / Hari Raya Adilfitri (Islam) 
Chinese New Year (Chinese) 
Deepavali (Indian) 
Christmas Day (Christian)

Landmarks
Petronas Twin Towers, Kuala Lumpur

Local Delicacies - 
	Roti Canai
	Satay
	Nasi Lemak
	Acar
	Teh Tarik
	Bak Kut Teh
	Asam Laksa
	Ipoh White Coffee',
            'country_code'   	=> 'MYS',
            'organisation_id'	=> '1'
        ]);

        Information::create([
            'information'		=> 'Capital City: Beijing
Largest City: Shanghai
Official Language: Standard Chinese
Population: 1,357,380,000
GDP: $10.38 trillion
Currency: Renminbi (yuan) (CNY) (¥)
Time zone: UTC+8
States: 23 provinces, 4 Municipalities, 5 Autonomous regions, 2 Special administrative regions
Religion: Three teachings: Confucianism, Buddhism, Taoism
Ethnic group: China officially recognises 56 distinct ethnic groups, of which the largest one is the Han Chinese, who constitute about 91.51% of the total population.
Festival: There are lots of festivals in China reflecting both culture and tradition of this nation. Several ones are listed below.
- Spring festival: Spring festival is on the 1st day of the 1st lunar month. And it is the most important festival for Chinese people. Normally, family members, including ones living in distance, will join together to celebrate the end of the former year and the beginning of the coming one. Like Christmas in West, Spring festival starts the night before it and will last till the mid 1st lunar month of the new year.
- Mid-Autumn festival: Mid-Autumn festival is on the 15th day of the eighth lunar month. It is the second important festival as far as most Chinese people considered. While it is coming, people will go back home to meet their family, admiring the full moon, having moon-cakes. So it is also known as the "day of Reunion" and the "Moon Festival".
- Qingming festival: This festival is also called Tomb Sweeping Day that is on April 4 or 5. Therefore, tomb sweeping, on this day, is one the most important activities to do to show respects to ancestors. Meanwhile, it is also a time for people to go outside and start enjoying the greenery of spring.
- Double Seventh festival: It is on, as the name of it shows, the seventh day of the seventh lunar month. In China, it is called Qixi Festival, which is Chinese Valentine\'s Day.',
            'country_code'   	=> 'CHN',
            'organisation_id'	=> '1'
        ]);

        Information::create([
            'information'		=> 'Capital City – Kinshasa
Popular Cities – Mbandaka, Goma, Bukavu
Largest City – Kinshasa
National Language - French
Official languages – Swahili, Kituba, Lingala, Tshiluba
Government – Semi presidential republic
Independence day – 30th June 1960
Population – 18th in the world (approx. 77 million)
GDP (PPP)– approx. $55 billion
GDP (nominal) – approx. $32 billion
Currency – Congolese Franc (CDF)
Political Split – 10 provinces & 1 city province (Kinshasa)
Major Religion – Christianity
Festivals – Christmas (Christianity), Ramzan (Islam)
Food – Cassava, Fufu, Moambe, Chikwanga',
            'country_code'   	=> 'COG',
            'organisation_id'	=> '1'
        ]);

        Information::create([
            'information'		=> 'Capital City – New Delhi
Popular Cities – Chennai, Mumbai, Kolkata, Bengaluru
Largest City – Mumbai
National Language - None
Official languages – Hindi and English + 22 regional languages
Government – Federal Parliamentary Constitutional Republic
Independence day – 15th August 1947
Republic Day – 26th January 1950
Population – 2nd in the world (approx. 1.2 billion)
GDP (PPP)– 3rd in the world (approx. $7.9 trillion)
GDP (nominal) – 7th in the world (approx. $2.3 trillion)
Currency – Indian rupee (INR)
Political Split – 29 states & 7 union territories
Official Religion – None; Secular Country
Festivals – Diwali (Hindu), Christmas (Christianity), Ramzan (Islam)
Food – Idli, Chapathi, Pav Bhaji, Sambhar',
            'country_code'   	=> 'IND',
            'organisation_id'	=> '1'
        ]);*/
    }

}