<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role list
    |--------------------------------------------------------------------------
    |
    | This is array holds the default configuration options used when system refers to
    | any role of users. This provides an easy change to the system rather than hard
    | hard code everywhere.
    |
    */
    'roles' => [
        '1' =>  'super',
        '2' =>  'admin',
        '3' =>  'staff',
        '4' =>  'teacher',
        '5' =>  'student'
    ],

    'super' => [
    	'id'	=> 1,
    	'display_name'	=> 'Super Administrator',
        'link'  => 'super'
    ],

    'admin' => [
    	'id'	=> 2,
    	'display_name'	=> 'Administrator',
        'link'  => 'admin'
    ],

    'staff' => [
    	'id'	=> 3,
    	'display_name'	=> 'School Staff',
        'link'  => 'staff'
    ],

    'teacher' => [
    	'id'	=> 4,
    	'display_name'	=> 'Teacher',
        'link'  => 'teacher'
    ],

    'student' => [
    	'id'	=> 5,
    	'display_name'	=> 'Student',
        'link'  => 'student'
    ],

    'super_id' => 1,

    'admin_id' => 2,
    
    'staff_id' => 3,
    
    'teacher_id' => 4,
    
    'student_id' => 5
];
